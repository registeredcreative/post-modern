<?php
/**
 * A wrapper around WordPress' custom post type functionality.
 *
 * Simplifies the handling of custom post types by translating a straightforward
 * configuration array into the callbacks on actions and filters that power WordPress
 *
 *
 * @package PostModern
 * @subpackage PostModernType
 * @since 0.0.1
 */
class PostModernType extends PostModernObject{

    /**
     * singular name, in display style.
     * @var string
     */
    var $name = null;

    /**
     * plural name, display style
     * @var string
     */
    var $plural_name = null;

    /**
     * the post_type identifier to supply to wordpress. if null, automatically built from $name
     * @var string
     */
    var $post_type = null;

    /**
     * taken right from register_post_type, see @link http://codex.wordpress.org/Function_Reference/register_post_type
     * @var string
     */
    var $args = array();
    /**
     * the location of an icon sprite and the offset into it.
     * @var array
     */
    var $icon = null;
    /**
     * messages array to pass set up in the post_updated_messages callback
     * @var string
     */
    var $messages = array();

    /**
     * custom fields for this post type
     * @var array
     */
    var $post_meta = array();

    /**
     * automatically set up meta box for custom fields, boolean or array (id,title,context,priority)
     * @var mixed
     */
    var $auto_meta_box = true;
    /**
     * An associative array that describes the meta_boxes to create.
     *
     * @var mixed
     */
    var $meta_boxes = array();

    /**
     * file path of the 'post_type' contextutal help template
     * @var string
     */
    var $help = null;

    /**
     * file path of the 'edit-post_type' contextutal help template
     * @var string
     */
    var $edit_help = null;

    /**
     * Array of columns to display on the post listings admin page (edit-post_type, as given in filter
     * @link http://codex.wordpress.org/Plugin_API/Filter_Reference/manage_edit-post_type_columns
     *
     * @var array
     */
  	var $columns = null;

    /**
     * Array of columns to make sortable on the post listings admin page (edit-post_type, as given in filter
     *
     * @var array
     */
  	var $sortable_columns = null;

    /**
     * Array of taxonomies to filter on the post listings admin page (edit-post_type)
     * Optionally, could be the string 'all' which will create a filter for all related
     * taxonomies.
     * Taxonomies may be a string representing the 'taxonomy_name' or an args array to pass to
     * wp_dropdown_categories
     * @link http://codex.wordpress.org/Function_Reference/wp_dropdown_categories
     *
     * @var mixed
     */
  	var $filters = null;

    /**
     * Whether or not this object has been configured
     * @var bool
     */
    var $__configured = false;

    /**
     * the list of post modern field objects
     * @var array
     */
    var $__fields = array();

    /**
     * the list of registered taxonomies, as returned by get_taxonomies
     * @var array
     */
    var $__taxonomies = array();

    /**
     * the list of existing custom fields, as returned by get_post_custom, indexed by post_ID
     * @var array
     */
    var $__post_custom = array();
    /**
     * the list of columns, ready to merge in on 'manage_edit-<post_type>_sortable_columns'
     * @var array
     */
    var $__sortable = array();
    /**
     * the list of existing custom fields, as returned by get_post_custom, indexed by post_ID
     * @var array
     */
    var $__sort_column_types = array();

    /**
     * Initialize the object. Object must have config by this point. Called on action 'init'.
     * Registers filter 'post_updated_messages'
     *
     */
    function init(){
        // if it hasn't been setup, we need to quit
        if ( !$this->__configured ) return;

        // most of these are pretty self explanatory
        $this->setup_identifier();
        $this->setup_preferences();
        $this->setup_labels();
        if( $this->is_post_screen() || $this->is_edit_posts_screen() ){
	        $this->setup_post_meta();
	    }
        $this->setup_columns();
        $this->setup_filters();
        $this->setup_help();
        // fix the messages
        add_filter('post_updated_messages', array( &$this, 'update_messages') );
        // all done, let's register this bad boy
       	$this->register();
    }

    function admin_init(){
        add_action('posts_selection',array(&$this,'validate'));
        // are we rocking a custom icon?
        if( $this->icon ) add_action('admin_head',array(&$this,'render_icon_styles'));
    }

    /**
     *  Handles the matching up of var's id and post_type. Creates one from name if necessary.
     *
     */
    function setup_identifier(){
        // if ID supplied, set the post_type
        if( $this->id ) $this->post_type = $this->id;
        // if post_type supplied, set the id
        else if( $this->post_type ) $this->id = $this->post_type;
        // if neither specified, build it from the name
        else if( $this->name ) $this->post_type = $this->id = PostModern::sanitize_identifier( $this->name );

        if( !$this->plural_name ) $this->plural_name = $this->name;
    }

    /**
     *  Sets up behaviors of handling post_types that are preferences.
     *
     */
    function setup_preferences(){
        // preference: archives view is slug plural by default
        if( !isset($this->args['has_archive']) )
            $this->args['has_archive'] = PostModern::sanitize_identifier( $this->plural_name );

        // preference: auto-load meta data
        // causes problems for post_get
	    // add_action('posts_selection', array(&$this, 'load_meta_data') );

    }

    /**
     * Handles writing default labels based on the name of the object.
     * Overrides WordPress' generic default labels while still allowing the
     * overriding any (or all) specific message.
     *
     */
    function setup_labels(){
        // recreate WordPress' default labels using the name of the object
        $defaults = array(
            'name' => _x( $this->plural_name, $this->plural_name),
            'singular_name' => _x($this->name, $this->name),
            'add_new' => _x('Add New', strtolower($this->post_type) ),
            'add_new_item' => __('Add New '.$this->name),
            'edit_item' => __('Edit '.$this->name),
            'new_item' => __('New '.$this->name),
            'all_items' => __('All '.$this->plural_name),
            'view_item' => __('View '.$this->name),
            'search_items' => __('Search '.$this->plural_name),
            'not_found' =>  __('No '.strtolower($this->plural_name).' found'),
            'not_found_in_trash' => __('No '.strtolower($this->plural_name).' found in Trash'),
            // TODO: Verify if we need a label here when post heirachrical = true
            'parent_item_colon' => '',
            'menu_name' => $this->plural_name
        );
        // look for a conforming labels array to merge in
        if( isset($this->args['labels']) && is_array( $this->args['labels'])){
            $this->args['labels'] = array_merge($defaults, $this->args['labels']);
        } else { // otherwise just use the defaults
            $this->args['labels'] = $defaults;
        }
    }

    /**
     * Handles setting up the environment for the custom fields requested.
     * Registers callback for 'save_post' and adds a 'register_meta_box_cb' if
     * one is not supplied.
     *
     */
    function setup_post_meta(){
        if( !empty( $this->post_meta ) ){
	    	// if they haven't overriden the register_meta_box_cb, use ours
	    	if( !isset($this->args['register_meta_box_cb']) )
				$this->setup_meta_boxes();

			foreach( $this->post_meta as $id => &$field ){
				// if the id is not set, set it with the key
				if( !isset($field['id']) )
					$field['id'] = $id;
				// instantiate the field type
				$this->__fields[$id] = $this->new_field($field['type'], $field );
			}
			// add our save action
			add_action('save_post', array( &$this, 'save' ) );
        }
    }
    /**
     * Handles setting up the necessary meta boxes (including the auto-box).
     *
     */
    function setup_meta_boxes(){

        // if we are using the auto box, do some prep
        if( $this->auto_meta_box ){
            // if auto-box is boolean true, turn it into an array
            if( !is_array( $this->auto_meta_box ) ) $this->auto_meta_box = array();

            // findout what fields are already used so we don't render them
            $used = array();
            foreach( $this->meta_boxes as $mb ){
				foreach( $mb['fields'] as $f ){
					if( is_array($f) && is_array($f['fields']) )
						$used = array_merge( $used, $f['fields'] );
					else
						$used[] = $f;
				}
            }

            // if no fields, set some up
            if (empty($this->auto_meta_box['fields'])) {
                $fields = array();
                foreach ($this->post_meta as $key => $field ) {
                    if( !in_array($key, $used) )
                        $fields[] = $key;
                }
            }
            if (!empty($fields)) {
                // merge in the auto-box defaults
                $this->auto_meta_box = array_merge(
                    array(
                        'id' => $this->post_type.'_auto',
                        'title' => __($this->name.' Information'),
                        'context' => 'normal',
                        'priority' => 'high',
                        'fields' => $fields
                    ),
                    $this->auto_meta_box
                );
    			$this->meta_boxes[$this->post_type.'_auto'] =& $this->auto_meta_box;
            }
        }
        // if we have at least one metabox set the callback (overwrites the args array)
        if( !empty($this->meta_boxes) )
        	$this->args['register_meta_box_cb'] = array( &$this, 'render_meta_boxes' );

        // set up all of the metaboxes we have registered
        foreach( $this->meta_boxes as $metabox_id => &$metabox ){

            $metabox = array_merge(
                array(
                    'context' => 'normal',
                    'priority' => 'default'
                ),
                $metabox
            );

            if( empty($metabox['id']) ){
                $metabox['id'] = ( is_numeric($metabox_id) )? 'meta_box_'.$metabox_id : $metabox_id;
            }
            if( empty($metabox['title']) ){
                $metabox['title'] = PostModern::humanize_identifier($metabox['id']);
            }
        }
    }

    /**
     * Loads the meta data for the current post. Called on action 'posts_selection'.
     *
     */
    function load_meta_data(){
        if( !empty( $this->post_meta ) ){
			// get the values for this post's meta fields
			global $post;

			// first, check if we are the right post_type
			if( $post->post_type === $this->post_type ){
				// get all the values
				$field_values = get_post_custom($post->ID);

				foreach( $this->post_meta as $id => $field ){
					// if it already has a value, set it up here
					if( isset($field_values[ $id ]) ){
						$value = $field_values[ $id ];
						if( is_array( $value ) ){
							$n = count( $value );
							if( $n == 1 ) $value = $value[0];
							else if( $n == 0 ) $value = '';
						}
						$this->__fields[$id]->set( $value );
					}
				}
			}
		}
    }

    /**
     * Registers the callbacks for controlling the data displayed on the edit-post_type
     * screen (admin post listings). Registers 'manage_edit-post_type_columns' and
     * 'manage_post_type_custom_column'
     *
     */
    function setup_columns(){
        if( !empty($this->columns) ){
            add_action( 'manage_edit-'.$this->post_type.'_columns' , array( &$this, 'add_custom_columns') );
			add_action( 'manage_'.$this->post_type.'_posts_custom_column' , array( &$this, 'add_custom_column_values') );
        }
        /*
         *   sortable columns is expreimental, thus far
         */
		if( !empty($this->sortable_columns) ){
			$this->process_sortable_columns();
			add_filter( 'manage_edit-'.$this->post_type.'_sortable_columns', array( &$this, 'add_sortable_columns') );
			add_filter('request', array( &$this, 'add_sort_parameter') );
		}
		/* end exprimental feature */
    }
    /**
     * Registers the callbacks for filtering/restricting posts on the edit-post_type
     * screen (admin post listings). Registers 'manage_edit-post_type_columns' and
     * 'manage_post_type_custom_column'
     *
     */
    function setup_filters(){
        if( !empty($this->filters) ){
			add_action( 'restrict_manage_posts', array( &$this, 'add_post_filters_dropdown') );
			add_filter( 'parse_query', array( &$this, 'add_post_filters_query') );
		}
    }
    /**
     * Rewrites the messaging for this post type, overwriting WordPress' generic messaging.
     * Called on filter 'post_updated_messages'.
     *
     */
    function update_messages($messages){
        global $post, $post_ID;

        $defaults = array(
            0 => '', // Unused. Messages start at index 1.
            1 => sprintf( __($this->name.' updated. <a href="%s">View '.strtolower($this->name).'</a>'), esc_url( get_permalink($post_ID) ) ),
            2 => __($this->name.' updated.'),
            3 => __($this->name.' deleted.'),
            4 => __($this->name.' updated.'),
            /* translators: %s: date and time of the revision */
            5 => isset($_GET['revision']) ? sprintf( __($this->name.' restored to revision from %s'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
            6 => sprintf( __($this->name.' published. <a href="%s">View '.strtolower($this->name).'</a>'), esc_url( get_permalink($post_ID) ) ),
            7 => __($this->name.' saved.'),
            8 => sprintf( __($this->name.' submitted. <a target="_blank" href="%s">Preview '.strtolower($this->name).'</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
            9 => sprintf( __($this->name.' scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview '.strtolower($this->name).'</a>'),
                    // translators: Publish box date format, see http://php.net/date
                    date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
            10 => sprintf( __($this->name.' draft updated. <a target="_blank" href="%s">Preview '.strtolower($this->name).'</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
        );
		// overwrite any user defined messages
        $overwrite = array_keys( $this->messages );
        foreach( $overwrite as $index ){
        	$defaults[$index] = $this->messages[$index];
        }
        $messages[$this->post_type] = $defaults;

        return $messages;
    }

    /**
     * Registers the post type.
     *
     */
    function register(){
        if ( $this->__registered ) return false;

        register_post_type($this->post_type, $this->args);

        $this->__registered = true;
    }

    function validate(){
        global $current_screen,$post;
        static $message_added = false;

        // check to see if we are on the right screen first
        if( !isset($current_screen->id) || $current_screen->id != $this->post_type ) return;

        if( isset($post->post_status) && $post->post_status == 'publish' ){
            $class = 'error';
            $message = 'Please correct the errors below. This '.strtolower($this->name).
                       ' has been published and may not display correctly.';
        } else {
            $class = 'updated';
            $message = 'Please correct the errors below before publishing this '.strtolower($this->name).'.';
        }

        if( $message_added ) return;

        foreach( $this->__fields as &$field ){
            if( !$field->validate() ){
                $this->add_admin_notice(
                    $class,
                    __($message)
                );
                $message_added = true;
                return;
            }
        }
    }

    /**
     * Handles saving all of the custom fields for this post type. Called on action
     * 'save_post'
     *
     */
    function save(){
		global $post;
        // if this is a new post, don't bother saving
        if( $post == null ) return;

        // verify this came from our screen and with proper authorization,
        // because save_post can be triggered at other times
        if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;


		// verify nonce for all of the metaboxes
        if( !empty( $this->meta_boxes ) ){
            foreach( $this->meta_boxes as $metabox ){
                $nonce_key = $metabox['id'].'_nonce';
                if( !empty($_POST[ $nonce_key ]) ){
	                if ( !wp_verify_nonce( $_POST[ $nonce_key ], plugin_basename( __FILE__ ) ) ) {
	                    // TODO: Log the error
	                    trigger_error('Unable to verify WordPress nonce field "'.$nonce_key.'". Supplied value "'.$_POST[$nonce_key].'"');
	                    return;
	                }
	            } else {
	            	// something wacky going on, could be an attack
	            	return;
	            }
            }
        }

		$this->check_authorization();


        foreach( $this->post_meta as $id => $field ){
        	// make sure there is a PostModerField before calling save
			if( $this->is_field($this->__fields[$id]) )
				$this->__fields[$id]->save( $post->ID );
	    }
    }
    /**
     * Checks to make sure that the current user has permission to edit this post type.
     *
     */
    function check_authorization(){
        // how complicated does this need to be?

		$post_type = get_post_type_object($this->post_type);
		// TODO: handle edit_others_posts case
		if( current_user_can( $post_type->cap->edit_posts ) ) return;

		wp_die(
			__('You are not authorized to edit this '.strtolower($this->name).'.'),
			'Authorization Failure',
			array(

			)
		);

    }

    /**
     * Render icon styles expects a 64x64 pixel icon sprite where the...
     **/
    function render_icon_styles(){
        if( is_array($this->icon) ){
            list($url,$offset) = $this->icon;
        } else {
            $url = $this->icon;
            $offset = 0;
        }

        global $post;
        $root_selector = '#adminmenu #menu-posts-'.$this->post_type;

        // open up the styles
        echo '<style type="text/css">';
        // output the active version
        $x = $offset + 1; // wordpress sprites are 30x30 pixel boxes; ours are 32x32. Add half the difference
        $y = 1; // accomodate the difference between a 32pixel sprtie and a 30.
        echo $root_selector.':hover div.wp-menu-image,'.
             $root_selector.'.wp-has-current-submenu div.wp-menu-image{background: transparent url('.$url.') no-repeat -'.$x.'px -'.$y.'px;}';
        // output the inactive version
        $y = 33; // 32 + 1
        echo $root_selector.' div.wp-menu-image{background: transparent url('.$url.') no-repeat -'.$x.'px -'.$y.'px;}';
        // if on the page, the large icon in the page header
        //if( $post && $post->post_type == $this->post_type ){
            $x -= 1;
            $y = 64;
            echo '#icon-edit.icon32-posts-'.$this->post_type.'{background: url('.$url.') no-repeat -'.$x.'px -'.$y.'px;}';
        //}
        // tie it off
        echo '</style>';
    }

    /**
     * Adds callback to render each meta box. Called on
     * action 'add_meta_boxes'
     *
     */
    function render_meta_boxes(){

        foreach( $this->meta_boxes as $metabox ){
            if( !empty($metabox['fields']) ){
                // call WordPress to add the auto-box
                add_meta_box(
                    $metabox['id'],
                    $metabox['title'],
                    array( &$this, 'render_meta_box' ), // the render call back
                    $this->post_type,
                    $metabox['context'],
                    $metabox['priority'],
                    array(
                    	'context' => $metabox['context'],
                    	'fields' => $metabox['fields'] // the fields
                    )
                );
            }
        }
    }
    /**
     * Renders a single meta box.
     *
     */
    function render_meta_box($post,$metabox){

		$context = '';
		$fields = array();
		extract($metabox['args']); // should set $context and $fields

        // Use nonce for verification
        wp_nonce_field( plugin_basename( __FILE__ ), $metabox['id'].'_nonce' );
        $class = 'pomo_metabox context_'.$context; // need metabox context here
        echo '<div class="'.$class.'">';
        if( isset($this->meta_boxes[ $metabox['id'] ]['message']) )
        	echo '<p class="metabox_message">'.
	        		$this->meta_boxes[ $metabox['id'] ]['message'].
	        	 '</p>';

        foreach( $fields as $field ){
            // if it is an array, it is a fieldset
            if( is_array($field) && is_array($field['fields']) ){
            	echo '<fieldset>';
            	echo (!empty($field['legend']) )? '<legend>'.$field['legend'].'</legend>' : '';
            	echo (!empty($field['message']) )? '<p class="fieldset_message">'.$field['message'].'</p>' : '';
            	foreach( $field['fields'] as $f )
					$this->render_field($f,$context);
            	echo '</fieldset>';
            } else {
            	// it's just a field
				$this->render_field($field,$context);
            }
        }

        echo '</div><!-- .pomo_metabox -->';
    }
    /**
     * Renders a single field.
     *
     */
	function render_field($field_id,$context){
		if( isset( $this->__fields[$field_id]) )
			$this->__fields[$field_id]->render_form(false,$context);
	}

    /**
     * Instantiates the requested field type (child of PostModernField or PostModernComboField). Falls back to
     * TextField if class not found.
     *
     * @param string $type The field type idenifier (text, radio, checkbox, select, textarea, etc.)
     * @param array $config An associative array containing the field configuration data
     *
     */
 	function new_field( $type, $config ){
        // convert the identifier
 		$class_name = PostModern::camelize_identifier($type);

 		if( class_exists( $class_name.'ComboField' ) )
            $class_name .= 'ComboField';
 		else if( class_exists( $class_name.'Field' ) )
            $class_name .= 'Field';
 		else{
			// fallback to a textfield
			$class_name = 'TextField';
 		}
		return new $class_name($this->post_type,$config);
 	}
    /**
     * Determines whether the passed object is a child of 'PostModernField'.
     *
     * @param object $object An object (not class) to test
     */
 	function is_field( $object ){
 		if( !$object ) return false;

 		//return is_a($object,'PostModernField') || is_a($object,'PostModernComboField');
 		return $object instanceof PostModernFieldInterface;
 	}

    /**
     * Replaces the deafult columns with the custom list. Called on filter 'manage_edit-post_type_columns'
     * @link http://codex.wordpress.org/Plugin_API/Filter_Reference/manage_edit-post_type_columns
     *
     *
     * @param array $columns A list of column keys comprizes of built-ins, taxonomies, and fields
     */
    function add_custom_columns($columns){
        // load up the taxonomies so that we can check them in each column
        $this->__taxonomies = get_taxonomies();
		if( is_array($this->columns) ) $columns = $this->columns;
		return $columns;
	}

    /**
     * Outputs the appropriate value for the column. Called on action 'manage_post_type_posts_custom_column'
     *
     * @param string $column_key The idenifier of the column that needs output
     */
    function add_custom_column_values($column_key){
		global $post;
		if( !isset($this->__post_custom[$post->ID] ) )
	        // load up the custom fields so that we can check them in each column
	        $this->__post_custom[$post->ID] = get_post_custom();
		// check if it is a custom field
		if( array_key_exists($column_key,$this->__post_custom[$post->ID]) ){
			// first get the value
            $value = $this->__post_custom[$post->ID][$column_key];

			// determine if it is a single value
			if( count($value) == 1 ) $value = array_pop( $value );

			// then check if we have the field to filter it before outputting
			if( isset( $this->__fields[$column_key] ) ){
                echo $this->__fields[$column_key]->get_column_value($value);
            } else {
                // implode will cover us if value is still an array
                if( is_array($value) )
	                echo implode(', ',$value);
            }

		} else if( array_key_exists($column_key,$this->__taxonomies) ){
			// else it might be a taxonomy
			$terms = get_the_term_list( $post->ID , $column_key , '' , ',' , '' );
			if ( is_string( $terms ) ) {
				echo $terms;
			}
		}
	}

    /**
     * Processes the declared sortable columns. Called on setup.
     *
     */
	function process_sortable_columns(){
		foreach( $this->sortable_columns as $key => $value ){
			if( is_array($value) ){
			     list( $value, $col_type ) = $value;
			} else {
			     $col_type = 'normal';
			}

			if( is_int($key) ){
			     $key = $value;
            }
            $this->__sortable[$key] = $value;
            $this->__sort_column_types[$key] = $col_type;

		}
	}

    /**
     * Replaces the array of column keys to make sortable. Called on filter 'manage_edit-<post_type>_sortable_columns'
     *
     */
	function add_sortable_columns($columns){
		return array_merge($columns,$this->__sortable);
	}

	/**
     * Detects if the orderby request parameter is a declared (as opposed to actual) meta_key. Called on filter 'request.'
     * @param array $vars The query vars for this request, passed in by filter.
     * @return array $vars Modified with new sort parameter
     */
	function add_sort_parameter($vars){
        if( !empty($vars['orderby']) && array_key_exists($vars['orderby'],$this->post_meta) ){
            // this could be further abstracted to use the col_type in a hash, but for only two values, it's fine like this.
            if( 'numeric' === $this->__sort_column_types[ $vars['orderby'] ] ){
                $col_type = 'meta_value_num';
            } else {
                $col_type = 'meta_value';
            }
            $vars = array_merge( $vars, array(
                'meta_key' => $vars['orderby'],
                'orderby' => $col_type
            ));
        }
        return $vars;
	}

	/**
	 * Adds custom taxonomy filters to the post index screen for this post type.
	 *
	 */
	function add_post_filters_dropdown(){
	    global $typenow;

		if( $typenow != $this->post_type ) return;

    	if( $this->filters == 'all' )
	    	$filters = get_object_taxonomies( $typenow );
    	else $filters = $this->filters;

        foreach ( $filters as $tax_type ) {
			if( is_array($tax_type) ) {
				$tax_type['selected'] = !empty($_GET[$tax_type['taxonomy']])? $_GET[$tax_type['taxonomy']] : '';
				wp_dropdown_categories( $tax_type );
			} else{
	            $tax = get_taxonomy( $tax_type );
				$selected = !empty($_GET[$tax_type])? $_GET[$tax_type] : '';
	            $list = wp_dropdown_categories(
	            	array(
		                'show_option_all' => __('Show All '.$tax->label ),
		                'taxonomy' => $tax_type,
		                'name' => $tax->name,
		                'orderby' => 'name',
		                'selected' => $selected,
		                'hierarchical' => $tax->hierarchical,
		                'hide_empty' => false,
		                'echo' => false
	            	)
	            );
	            // check to make sure there are options before outputting empty selector
	            if( strpos($list,'<option') !== false ){
	               echo $list;
	            }
	       }
        }
	}

	function add_post_filters_query( $query ) {
		global $pagenow, $typenow;

		if ( 'edit.php' == $pagenow ) {

	    	if( $this->filters == 'all' )
		    	$filters = get_object_taxonomies( $typenow );
	    	else $filters = $this->filters;

			foreach ( $filters as $tax_type ) {
				// if it is an array, strip out the tax_type
				if( is_array($tax_type) ) {
					if( empty($tax_type['taxonomy']) ) continue;
					$tax_type = $tax_type['taxonomy'];
				}

				if( !empty($query->query_vars[$tax_type]) ){
					$var = &$query->query_vars[$tax_type];
					$term = get_term_by( 'id', $var, $tax_type );
					$var = $term->slug;
				}
			}
		}
	}

	/**
	 * Determines if the current screen is the post screen. When possible, it will determine
	 * if it is the post screen for this type of post.
	 *
	 */
	function is_post_screen(){
		global $pagenow,$post;

		if( !empty($_POST) ){
			// this test tells us if we are saving one of our posts.
			if( !empty( $_POST['action'] ) && $_POST['action'] == 'editpost' ){
				if( !empty($_POST['post_type']) && $_POST['post_type'] == $this->post_type )
					return true;
			}
			return false;
		} else if ( !empty($post) ){
			return $this->post_type = $post->post_type;
		} else if( $pagenow == 'post-new.php' ){
			// this test tells us if we are creating a new one of these types
			if( !empty( $_GET['post_type'] ) ){
				if ( $_GET['post_type'] != $this->post_type )
					return false;
			}
			// else we have to return true as we have no other way to rule out other post types.
			return true;
		} else if( $pagenow == 'post.php' ){
            return true;
		}
		return false;
	}

	function is_edit_posts_screen(){
		global $pagenow;

		if( $pagenow == 'edit.php' ){
			if( !empty($_GET['post_type']) )
				if( $_GET['post_type'] == $this->post_type )
					return true;
		}

		return false;
	}

}

