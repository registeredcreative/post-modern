<?php
/**
 * Base Class for PostModern Field Objects 
 *
 * Creates an abstract base and implements common functionality of PostModern custom fields (text,checkbox,radio,etc).
 * 
 *
 * @package PostModern
 * @subpackage PostModernField
 * @since 0.0.1
 */
abstract class PostModernField implements PostModernFieldInterface{

    /**
     * The unique identifier for this field. Referred to as 'type' elsewhere in the system. 
     * @var string
     */
	var $id = null;
    /**
     * An alias of id. 
     * @var string
     */	
	var $type = null;
    /**
     * The post_type of the parent object.   
     * @var string
     */		
	var $post_type = null;
    /**
     * Associative array that describes the configuration of this field.
     * @var array
     */	    
	var $config = array(
		'id' => '',
		'label' => '',
		'hint' => '',
		'class' => '',
		'input_class' => '',
		'validate' => '',
		'pre_save_callback' => '',
		'save_callback' => '',
		'sanitize_callback' => '',
		'validate_callback' => '',
		'value' => '',
		'is_array' => false
	);
    /**
     * the default vlaues for array fields
     * @var array
     */	
	var $array_defaults = array(
	   'clonable' => true, // whether or not the field should add 'clonable' class
	   'return_array' => true // whether the field should return an array from render i/o string
	);
    /**
     * the additional options a child field might have
     * @var array
     */	
	var $options = array();	
    /**
     * a predetermined string to use as the input name
     * @var string
     */	
	var $input_name = null;	
    /**
     * a predetermined string to use as the dom ID
     * @var string
     */	
	var $input_id = null;		
	/**
	 * boolean valid value of this field
	 * @var bool
	 */
	var $__is_valid = null;
    /**
     * the error to display when this field is not valid..   
     * @var string
     */		
	var $__validate_error = null;	
	
    /**
     * Construct the object.
     *
     * @param string $post_type Post type of the  
     * @param array $config Associative array that describes the configuration of the object 
     */
	function __construct($post_type, $config){
		$this->post_type = $post_type;
		$this->type =& $this->id;
		//first, process the config
		if( is_array($config) ){
			$this->config = array_merge( $this->options, $this->config, $config );
        }
        // now we have process the is_array config option seperately
        if( $this->config['is_array'] === true ){
            $this->config['is_array'] =& $this->array_defaults;
        } else if( is_array($this->config['is_array']) ){
            $this->config['is_array'] = array_merge($this->array_defaults,$this->config['is_array']);
        }

		$this->init();
		$this->check_id();
		
		add_action('admin_init', array(&$this,'admin_init'));
	}
	
    /**
     * Callback that allows subclasses to do work when the object is initialized.
     *
     */	
	function init(){
	
	}

    /**
     * Callback that allows subclasses to do work on admin init.
     *
     */	
	function admin_init(){
	
	}
	
    /**
     * Ensure that there is an id by creating one from the 'label' if necessary
     *
     */
	function check_id(){
		if( !$this->config['id'] ){ 
			// if there is a label, use it to create an id
			if( $this->config['label'] )
				$this->config['id'] = PostModern::sanitize_identifier( $this->config['label'] );
		}
		$this->config['id'] = trim($this->config['id']);
	}
    /**
     * get the form input name
     *
     */
	function get_input_name(){
		if( !empty($this->input_name)) return $this->input_name;
		
		$name = $this->post_type.'['.$this->config['id'].']';
		if( !empty($this->config['is_array']) && $this->config['is_array'] ) $name .= '[]';
		return $name;
	}
    /**
     * get the form input dom ID
     *
     */
	function get_input_id(){
		if( !empty($this->input_id)) return $this->input_id;
		
		$id = $this->post_type.'_'.$this->config['id'];
		if( !empty($this->config['is_array']) && $this->config['is_array'] ) $id .= '[]';
		return $id;
	}
    /**
     * get the posted value
     *
     * @return mixed the posted value of this field
     */
    function get_posted_value(){
    	if( !empty($_POST[$this->post_type][$this->config['id']]) )
        	$value = $_POST[$this->post_type][$this->config['id']];
        else $value = '';
    	// TODO: sanitize the input before passing it all around kingdom come
    	return $value;
    } 
    /**
     * Get the value of this field. Checks cache first, then loads from DB.
     *
     * @param int $post_id The id of the post to retieve the value. Optional, will default to the current post.
     * @return mixed the value of the current field object.
     */
	function get($post_ID=null){
		if( !$post_ID ){
			global $post_ID;
		}
		// if we haven't already retrieved it, do it now. Only going back to the database once throughout the request.
		if( empty($this->config['value']) ){
			$this->config['value'] =  $this->extract( get_post_meta( $post_ID, $this->config['id'] ) );
			$this->post_get();
		}
		return $this->config['value'];
	}
    /**
     * Extracts the value for this field from the results of get_post_custom or get_post_meta.
     *
     * @param array $post_custom the results of get_post_custom to parse
     * @return mixed The value of this field  
     */
    function extract($post_custom){
        if( isset($post_custom[ $this->config['id'] ]) ){
            $value = $post_custom[ $this->config['id'] ];
        } else {
        	$value = $post_custom;
        }
        
        if( empty($value) ) return '';
        // all post meta retrieved from get_post_custom are arrays
        if( count($value) == 1 ) $value = $value[0];
        return $value;
    }
    /**
     * Post-get callback allows subclasses to do work after the value is loaded from the DB without the need to  
     * override the get or extract functions. Only called after DB retrieval.
     *
     */	
	function post_get(){

	}
    /**
     * Set the value of this field
     *
     * @param mixed $value The value to associate with this field  
     */
	function set($value){
		$this->config['value'] = $value;
	}

    /**
     * Returns a short representative value appropriate for showing in the table in the edit-post_type screen.
     *
     * @param mixed $value the value to filter. 
     * @return mixed the value of the current field object.
     */
	function get_column_value($value){
		if( is_array($value) ) return implode(', ', $value);
		return $value;
	}
	
    /**
     * Saves the field from the POST data. 
     *
     * @param int $post_id The id of the post to retieve the value. Optional, will default to the current post.
     * @return bool true on success and false on failure 
     */	
	function save($post_ID=null){
		if( !$post_ID ){
			global $post_ID;
		}
        $this->config['value'] = $this->get_posted_value();
		$this->pre_save();
		$this->config['value'] = $this->sanitize( $this->config['value'] );
		
		// if the user has supplied a callback, call it for them
		if( !empty($this->config['save_callback']) ){
			if( is_callable($this->config['save_callback']) ){
				return call_user_func( $this->config['save_callback'], $this->config['value'] );// return the return value from the user function 				
			} else {
				trigger_error('Unable to call user function \''.$this->config['save_callback'].'\' to save \''.$this->config['id'].'\'', E_USER_WARNING);
			}
		}
		$success = false;
		// if empty, delete it
		if( empty($this->config['value']) ){
			$success = delete_post_meta( $post_ID, $this->config['id'] );
		} else {
            $success = $this->save_field($post_ID, $this->config['id'], $this->config['value']);
		}
        
		if( $success ) return true;

		return false;

	}
	
	/**
     * Store the field value in the database.
     *
     * @param int $post_id The id of the post to associate this field
     * @param string $field The meta_key for the field to save
     * @param mixed $value Array or string value to save
     * @return bool Whether or not the save was successful. In the case of arrays, will return whether all were successful
     */	
	function save_field($post_id, $field, $value){
        if( is_array($value) ){
			/*
			 * TODO: Be OK with array handling decision
			 * 
			 * In an array, we have two choices:
			 *			
			 *  	1.) Assume that these are the only values for this field and delete all previous (slightly dangerous)
			 *
			 *		2.) Get the previous values and merge them with the new values (ridiculously sloppy)
			 *		
			 * Going with option 1.
			 */ 
			delete_post_meta($post_id,$field);
			$success = true;
			// save the new values
			foreach( $value as $v ){
    			$v = trim($v);
    			if( $v ){
        			$success = add_post_meta( 
        				$post_id, 
        				$field, 
        				$v
        			);
        			if( $success === false ){
    					trigger_error('Unable to save multiple field, \''.$field.'\' with value \''.$v.'\' for post id: '.$post_id.' ', E_USER_NOTICE);
        			}
                }
    		}
		} else {
			$success = update_post_meta( 
				$post_id, 
				$field, 
				$value
			);
			if( $success === false ){
				//trigger_error('Unable to save single field, \''.$field.'\' with value \''.$value.'\'', E_USER_NOTICE);
			}
		}
		return $success;
	}
	
    /**
     * Pre-save callback allows subclasses to do work before the save takes place without the need to override 
     * the save function. Called before sanitize.
     *
     */	
	function pre_save(){

	}

    /**
     * Returns a sanitized value appropriate for this field. Can be overriden by a callback in the field config.
     *
     * @param mixed $value the value to sanitize. Optional, will default to the current value of the field.
     * @return mixed the sanitized value.
     */
	function sanitize($value){
		if( !empty($this->config['sanitize_callback']) ){
			if( is_callable($this->config['sanitize_callback']) ){
				return call_user_func( $this->config['sanitize_callback'], $value);// return the return value from the user function
			} else {
				trigger_error('Unable to call user function \''.$this->config['sanitize_callback'].'\' to sanitize \''.$this->config['id'].'\'', E_USER_WARNING);
			}
		}
		// call the clean method to do the work
		if( is_array($value) ){
			$values = array();
			foreach( $value as $k => $v ){
				$values[$k] = $this->clean($v);
			}
			return $values;
		}
		return $this->clean($value);
	}

    /**
     * Returns a clean version of the data according to the user's rules.
     *
     * @param mixed $value the value to clean.
     * @return mixed the sanitized value.
     */	
	function clean($value){
		return $value;
	}

    /**
     * Determines whether or not this field is valid. Can be overriden by a callback in the field config.
     *
     * @param mixed $value the value to validate. Optional, will default to the current value of the field.
     */
	function validate($value=null){
		if( $value == null ) $value = $this->get();
        global $post;
        
        // if it is a brand new post, don't validate it
        if( !is_object($post) || $post->post_status == 'auto-draft' ) return true;
		
		if( !empty($this->config['validate_callback']) ){
			if( is_callable($this->config['validate_callback']) ){
				// call the user function
				return call_user_func( $this->config['validate_callback'], $value); // return the return value from the user function 
			} else {
				// TODO: Log error
				trigger_error('Unable to call user function \''.$this->config['validate_callback'].'\' to validate \''.$this->config['id'].'\'', E_USER_WARNING);
			}
		} 
		// if validation has not been requested, bail happy
		if( empty($this->config['validate']) ) return true;
		// call the clean method to do the work
		if( is_array($value) ){
			$valid = true;
			foreach( $value as $v ){
				$valid = $this->is_valid($v);
			}
			return $valid;
		}
		return $this->is_valid($value);
	}
	
    /**
     * Determines whether or not this field is valid. 
     *
     * @param mixed $value the value to validate. Optional, will default to the current value of the field.
     */	
	function is_valid($value=null){
		if( $value == null ) $value = $this->get();
		
		if( $this->__is_valid != null ) return $this->__is_valid;
        $this->__is_valid = true;
		// this is where we check $this->config['validate']
		if( !empty($this->config['validate']) && is_array( $this->config['validate'] ) ){
    		foreach( $this->config['validate'] as $function => $args ){
        		if( !PostModern::validate( $function, $value, $args ) ){
            		if( is_array( $args ) && isset($args['error']) ) $this->__validate_error = $args['error'];
            		else if( !empty($this->config['error']) ) $this->__validate_error = $this->config['error'];
            		
            		$this->__is_valid = false;
                }
            }
        }
		return $this->__is_valid;		
	}
	
    /**
     * Returns a space separated list of CSS classes for the field wrapper (suitable for setting the class variable).
     *
     * @param string $class additional class(es) to add to the string
     * @return string the list of classes.
     */	
	function classes($class=null){
		global $post; 
		$post_status = ( $post )? $post->post_status : 'auto-draft';
		$clonable = ( $this->config['is_array']['clonable'] )? 'clonable':'';

		return implode( ' ', array(
			'form_field',
			$this->id.'_field',
			$this->post_type.'_meta_field',
			$post_status,
			$clonable,
			sanitize_html_class( $class )
		));
	}
	
    /**
     * Returns a space separated list of CSS classes for the input field itself (suitable for setting the class variable).
     *
     * @param string $class additional class(es) to add to the string
     * @return string the list of classes.
     */		
	function input_classes($class=null){
		return implode( ' ', array(
			'form_field_input',
			$this->id.'_input',
			$this->post_type.'_'.$this->id.'_input',
			sanitize_html_class( $class )
		));	
	}
	
	
    /**
     * Prints the HTML code necessary to edit this field inside of a form object.
     *
     * @param bool $return whether or not to return the output instead of printing it
     * @return string 
     */
	function render_form($return=false){}
	
}