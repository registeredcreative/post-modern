<?php

class ColorField extends PostModernField{

	var $id = 'color';
	
	var $options = array(
	);

	function admin_init(){
        wp_enqueue_style('farbtastic');
        wp_enqueue_script('farbtastic');
	}
	
	function pre_save(){
	}
	
	function render_form($return=false){
		$field = &$this->config;
		
		if( !($field['id'] || $field['label']) ){ 
			$output = __('Cannot create field with neither an ID nor a label.<br>');
			if( $return ) return $output;
			
			echo $output;
			return;
		}
		// if no label supplied, create one from the id
		$label = (!empty($field['label']))? $field['label'] : PostModern::humanize_identifier( $field['id'] );
		// get the value to output
		$value = (!empty($field['value']))? esc_attr(stripslashes($this->config['value'])) : '';
		// grab the DOM name and id
		$dom_id = $this->post_type.'_'.$this->config['id'];
		$input_name = $this->get_input_name();
		// wrap helptext with DOM structure (because that structure should not be output otherwise)
		$helptext = (!empty($field['hint']))? '<div class="helptext">'.$field['hint'].'</div>' : '';
		// add placeholder text
		$placeholder = (!empty($field['placeholder']))? ' placeholder="'.$field['placeholder'].'"' : '';
		// set up validation stuff
		if( !$this->validate($value) ){
          $valid_class = ' invalid ';
          $valid_message = '<div class="validation_error">'.$this->__validate_error.'</div>';
		} else {
		  $valid_class = '';
		  $valid_message = '';
		}
		// the list of classes to be output on the wrapper
		$classes = (!empty($field['class']))? $field['class']: '';
		$classes = $this->classes($classes).$valid_class;
		// the list of classes to be output on the input itself
		$input_classes = (!empty($field['input_class']))? $field['input_class']: '';
		$input_classes = $this->input_classes($input_classes).$valid_class;
        // maxlength attr of the input box
        $maxlength = ( isset($field['max_length']) && is_numeric($field['max_length']) )? ' maxlength="'.$field['max_length'].'"' : '';
				
		$output = '<div class="'.$classes.'">'.
		            $valid_message.
				 	'<label for="'.$dom_id.'">'.$label.'</label>'.
				 	'<input type="color" class="'.$input_classes.'" name="'.$input_name.'" id="'.$dom_id.'" value="'.$value.'"'.$placeholder.'>'.
				 	'<span class="color_sample" style="background:'.$value.'"></span>'.
				 	'<input type="button" value="Select a Color" class="color_picker_button button hide-if-no-js">'.
				 	'<div class="color_picker"></div>'.
				 	$helptext.
				  '</div><!-- #'.$dom_id.' -->';
				 
		if ( $return ) return $output;
		
		echo $output;
		
    }

}