<?php

class PostField extends SelectField{

	var $id = 'post';
	
	var $options = array(
		'options' => array(),
		'multiple' => false,
		'size' => '5',
		'add_new_link' => true,
		'args' => array(
			'numberposts' => 9999,
			'post_type' => 'post',
			'post_status' => 'publish'
		)
	);
	
	function init(){
        if( !empty($this->config['args'] ) )
			$this->config['args'] = array_merge($this->options['args'],$this->config['args']);
		else 
			$this->config['args'] = $this->options['args'];
			
		$this->get_posts();
		
		return;
	}
	
	function get_posts(){
		$posts = get_posts( $this->config['args'] );
		
		foreach( $posts as $post ){
		   $this->config['options'][$post->ID] = $post->post_title;
		}
	}

	
	function render_form($return=null){
        if( $this->config['add_new_link'] ){
        	$this->config['append'] = '<a href="#" class="new-post hide-if-no-js">+Add New</a>';
        	$output = $this->render_new_post_form();
        } else{
            $output = "";
        }
        
        parent::render_form();
	   
	    echo $output;
	}
	
	function render_new_post_form(){
		$output = '<div class="quick-add-post ready">'.
				  '<input type="text" value="" autocomplete="off" id="title" name="new-post-title" class="new-post-title">'.
				  '<input type="hidden" value="'.$this->config['args']['post_type'].'" name="new-post-type" class="new-post-type">'.
				  '<input type="hidden" value="'.$this->config['args']['post_status'].'" name="new-post-status">'.
				  '<input type="hidden" value="'.wp_create_nonce( WP_PLUGIN_DIR.'pomo-ajax-quick-add-post' ).'" name="new-post-nonce" class="new-post-nonce">'.
				  '<input type="button" value="'.__('Add').'" class="button quick-add-submit" name="new-post-add">'.
				  '<a href="#" class="cancel-action">'.__('Cancel').'</a>'.
				  '<img alt="" src="'.admin_url('images/wpspin_light.gif').'" class="waiting" style="display:none;">'.
				  '<p class="notice error" style="display:none;">'.__('Error saving new item').'</p>'.
				  '</div>';
				  
		return $output;
	}	
}