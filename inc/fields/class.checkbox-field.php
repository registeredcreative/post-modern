<?php

class CheckboxField extends PostModernField{

	var $id = 'checkbox';
	
	var $options = array(
		'checked_value' => 1
	);
	
	function get_column_value($value){
        $checked = '';        
        if( $value ){
            $checked = 'checked';
        }
        return '<input type="checkbox" '.$checked.' disabled />';
	}
	
	function render_form($return=false){
		$field = &$this->config;
		
		if( !($field['id'] || $field['label']) ){ 
			$output = __('Cannot create field with neither an ID nor a label');
			if( $return ) return $output;
			
			echo $output;
			return;
		}
		// if no label supplied, create one from the id
		$label = ($field['label'])? $field['label'] : PostModern::humanize_identifier( $field['id'] );
		// get the value
		$value = $field['checked_value'];
		// grab the DOM name and id
		$dom_id = $this->post_type.'_'.$this->config['id'];
		$input_name = $this->get_input_name();
		// wrap helptext with DOM structure (because that structure should not be output otherwise)
		$helptext = (!empty($field['hint']))? '<div class="helptext">'.$field['hint'].'</div>':'';
		// the list of classes to be output on the wrapper
		$classes = (!empty($field['class']))? $field['class']: '';
		$classes = $this->classes( $classes);
		// the list of classes to be output on the input itself
		$input_classes = (!empty($field['input_class']))? $field['input_class']: '';
		$input_classes = $this->input_classes( $input_classes );
		// determine the state of the checkbox
		$checked = ( !empty($field['value']) && $field['value'] )? ' checked' : '';

		$output = '<div class="'.$classes.'">'.
					// hidden field guarantess that at least one value shows up, breaks clickable label, however
					'<input type="hidden" name="'.$input_name.'" id="'.$dom_id.'" value="0">'. 
				 	'<label class="checkbox_label" for="'.$input_name.'">'.$field['label'].'</label>'.
				 	'<label class="wrapping_label">'.
						'<input type="checkbox" class="'.$input_classes.'" name="'.$input_name.'" id="'.$dom_id.'" value="'.$value.'" '.$checked.'>'.
				 		$helptext.
				 	'</label>'.
				 '</div><!-- #'.$dom_id.' -->';
				 
		if ( $return ) return $output;
		
		echo $output;
		
    }

}