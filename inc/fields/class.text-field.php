<?php

class TextField extends PostModernField{

	var $id = 'text';
	
	var $options = array(
	   'max_length' => false,
	   'preprend' => '',
	   'append' => '' 
	);
	
	function pre_save(){
	   if( $this->config['max_length'] ){
	       $this->set( substr( $this->get(), 0, $this->config['max_length'] - 1) );
	   }
	}
	
	function render_form($return=false){
		$field = &$this->config;
		
		if( !($field['id'] || $field['label']) ){ 
			$output = __('Cannot create field with neither an ID nor a label.<br>');
			if( $return ) return $output;
			
			echo $output;
			return;
		}
		// if no label supplied, create one from the id
		$label = (!empty($field['label']))? $field['label'] : PostModern::humanize_identifier( $field['id'] );
		// get the value to output
		$value = $this->get();
		// grab the DOM name and id
		$dom_id = $this->get_input_id();
		$input_name = $this->get_input_name();
		// get the value to display before the field
		$prepend = (!empty($field['prepend']))? '<span class="prepend">'.$field['prepend'].'</span>' : '';
		// get the value to display after the field
		$append = (!empty($field['append']))? '<span class="append">'.$field['append'].'</span>' : '';
		// wrap helptext with DOM structure (because that structure should not be output otherwise)
		$helptext = (!empty($field['hint']))? '<div class="helptext">'.$field['hint'].'</div>' : '';
		// add placeholder text
		$placeholder = (!empty($field['placeholder']))? ' placeholder="'.$field['placeholder'].'"' : '';
		// set up validation stuff
		if( !$this->validate($value) ){
          $valid_class = ' invalid ';
          $valid_message = '<div class="validation_error">'.$this->__validate_error.'</div>';
		} else {
		  $valid_class = '';
		  $valid_message = '';
		}
		// the list of classes to be output on the wrapper
		$classes = (!empty($field['class']))? $field['class']: '';
		$classes = $this->classes($classes).$valid_class;
		// the list of classes to be output on the input itself
		$input_classes = (!empty($field['input_class']))? $field['input_class']: '';
		$input_classes = $this->input_classes($input_classes).$valid_class;
        // maxlength attr of the input box
        $maxlength = ( isset($field['max_length']) && is_numeric($field['max_length']) )? ' maxlength="'.$field['max_length'].'"' : '';
        
				
		$format = '<div class="'.$classes.'">'.
		            $valid_message.
				 	'<label for="'.$dom_id.'">'.$label.'</label>'.
				 	$prepend.
				 	'<input type="text" class="'.$input_classes.' %s" name="'.$input_name.'" id="'.$dom_id.'" value="%s"'.$placeholder.'>'.
				 	$append.
				 	$helptext.
				  '</div><!-- #'.$dom_id.' -->';
		$output = array();		
		// do we also want to test if the user has requested an array field?
		if( is_array($value) ){
        	$i = 0;
        	foreach( $value as $v ){
        		if( $i > 0 && $field['is_array']['clonable'] ){
        			$class = 'clone';
        		} else {
        			$class = '';
        		}
        		$output[] = sprintf($format, $class, esc_attr(stripslashes($v)));
        		$i++;
        	}
        } else {
        	$output[] = sprintf($format, '', esc_attr(stripslashes($value)));
        }

        if( !$field['is_array'] || !$field['is_array']['return_array'] ){
            $output = implode('',$output);
        }

		if ( $return ) return $output;
		
		echo $output;
		
    }

}