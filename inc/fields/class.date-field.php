<?php

class DateField extends PostModernField{

	var $id = 'date';

	var $options = array(
	   'format' => 'm/d/Y', // php date format compatible string
	   'interpret' => true, // whether or not to interpret the string as a date 
	   'transform' => true  // whether or not to translate 
	);
	
	function init(){
	   if( !empty($this->config['validate']['between']) ){
	       list($after,$before) = $this->config['validate']['between'];
	       unset($this->config['validate']['between']);
	       if( $after && $before ){
	           $this->config['validate']['after'] = date( $this->config['format'], strtotime($after) );
	           $this->config['validate']['before'] = date( $this->config['format'], strtotime($before) );
	       }
	   } else {
	       // run before and after through the strtotime filter for interpretation
	       if( isset($this->config['validate']['after']) ) 
	           $this->config['validate']['after'] = date( $this->config['format'], strtotime($this->config['validate']['after']) ); 
	       if( isset($this->config['validate']['before']) ) 
	           $this->config['validate']['before'] = date( $this->config['format'], strtotime($this->config['validate']['before']) ); 
	   }
	}
	
	function admin_init(){
		wp_enqueue_style('post-modern-jquery-ui-custom', plugins_url('post-modern/js/libs/jquery/css/smoothness/jquery-ui-1.8.16.custom.css') );
    	wp_enqueue_script(
    	   'jquery-ui-date-picker',
    	    plugins_url('post-modern/js/libs/jquery/ui.date-picker.js'),
    	    array('jquery','jquery-ui-core'),
    	    '1.8.16',
    	    true
        );
	}
	
	
	function pre_save(){
        $t = strtotime( $this->config['value'] );
        
        if( $this->config['interpret'] && !empty($this->config['value']) ){
            if( $t ) $this->config['value'] = date( $this->config['format'], $t );
            else $this->config['value'] = '';
        }
        if( $this->config['transform'] && !empty($this->config['value']) ){
            if( $t ) $this->config['value'] = date( 'Y-m-d', $t );
        }
	}
	
    function post_get(){
        if( $this->config['transform'] && !empty($this->config['value']) ){
            $this->config['value'] = date( $this->config['format'], strtotime($this->config['value']) );
        }    
    }
    
    function get_column_value($value){
    	return date( $this->config['format'], strtotime($value) );
    }

	
	function render_form($return=false){
		$field = &$this->config;
		
		if( !($field['id'] || $field['label']) ){ 
			$output = __('Cannot create field with neither an ID nor a label.<br>');
			if( $return ) return $output;
			
			echo $output;
			return;
		}
		// if no label supplied, create one from the id
		$label = (!empty($field['label']))? $field['label'] : PostModern::humanize_identifier( $field['id'] );
		// get the value to output
		$value = $this->get();
		// grab the DOM name and id
		$dom_id = $this->post_type.'_'.$this->config['id'];
		$input_name = $this->get_input_name();
		// wrap helptext with DOM structure (because that structure should not be output otherwise)
		$helptext = (!empty($field['hint']))? '<div class="helptext">'.$field['hint'].'</div>':'';
		if( !$this->validate($value) ){
          $valid_class = ' invalid ';
          $valid_message = '<div class="validation_error">'.$this->__validate_error.'</div>';
		} else {
		  $valid_class = 'valid ';
		  $valid_message = '';
		}
		// the list of classes to be output on the wrapper
		$classes = (!empty($field['class']))? $field['class']: '';
		$classes = $this->classes($classes).$valid_class;
		// the list of classes to be output on the input itself
		$input_classes = (!empty($field['input_class']))? $field['input_class']: '';
		$input_classes = $this->input_classes($input_classes).$valid_class;
		
		// add the date format
		$format = $this->get_iso_date_format($field['format']);
		$field_data = ' data-format="'.$format.'"';
		$field_data .= ' data-constrain="'.(($field['interpret'])? '0':'1').'"';
		$field_data .= (!empty($field['validate']['before']))? ' data-maxdate="'.$this->add_days_to($field['validate']['before'], -1).'"' : '';
		$field_data .= (!empty($field['validate']['after']))? ' data-mindate="'.$this->add_days_to($field['validate']['after'], 1).'"' : '';
        $placeholder = ' placeholder="'.( !empty($field['placeholder'])? $placeholder : $format ).'"';
        
		$output = '<div class="'.$classes.'">'.
		            $valid_message.
				 	'<label for="'.$input_name.'">'.$label.'</label>'.
				 	'<input type="text" class="'.$input_classes.'" name="'.$input_name.'" id="'.$dom_id.'" value="'.$value.'"'.$field_data.$placeholder.'>'.
				 	$helptext.
				  '</div><!-- #'.$dom_id.' -->';
				 
		if ( $return ) return $output;
		
		echo $output;
		
    }
    
    function add_days_to($date,$days){
        return date($this->config['format'], strtotime($date) + (24*60*60)*$days );
    }
    
    /**
     * Takes an ISO style date format and converts it to a PHP date format.
     * @link http://docs.jquery.com/UI/Datepicker/formatDate
     * 
     * @param string $format the date format to convert (ex: mm/dd/yy)
     * @return string the php formated date string (ex: m/d/Y)
     */
    function get_php_date_format($format){
    	$map_1 = array(
			'dd' => '**d**', // day of month (two digit)
			'd' => 'j', // day of month (no leading zero)
			'oo' => 'z', // day of year (three digit) ** no PHP equiv
			'o' => 'z', // day of year (no leading zeros)
			'DD' => 'l', // day name long
			//'D' => 'D', // day name short
			'mm' => '**m**', // month of year (two digit)
			'm' => 'n', // month of year (no leading zero)
			'MM' => 'F', // month name long
			//'M' => 'M', // month name short
			'yy' => 'Y', // year (four digit) 
			//'y' => 'y', // year (two digit)
    	);
    	$map_2 = array(
			'**j**' => 'd', // **d** will get turned into **j** by the first map
			'**n**' => 'm', // **m** will get turned into **n** by the first map
    	);
    	
    	$tmp = str_replace( array_keys($map_1),array_values($map_1), $format );
    	return str_replace( array_keys($map_2),array_values($map_2), $tmp );    	
    }

    /**
     * Takes a PHP formatted date and converts it to an ISO notation.
     * @link http://docs.jquery.com/UI/Datepicker/formatDate
     * 
     * @param string $format the date format to convert (ex: m/d/Y)
     * @return string the js formated date string (ex: mm/dd/YY)
     */
    function get_iso_date_format($format){
    	$map = array(
			'd' => 'dd', // day of month (two digit)
			'j' => 'd', // day of month (no leading zero)
			'z' => 'o', // day of year
			'l' => 'DD', // day name long
			//'D' => 'D', // day name short
			'm' => 'mm', // month of year (two digit)
			'n' => 'm', // month of year (no leading zero)
			'F' => 'MM', // month name long
			//'M' => 'M', // month name short
			'Y' => 'yy', // year (four digit) 
			//'y' => 'y', // year (two digit)
    	);
    	    	
    	return str_replace( array_keys($map),array_values($map), $format );
    }

}