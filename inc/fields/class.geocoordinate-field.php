<?php
/**
 * Geo-coordinate Combo-Field  
 *
 * A combo field for geo-coordinates
 * 
 *
 * @package PostModern
 * @subpackage AddressField
 * @since 0.0.1
 */
class GeoPointField extends PostModernComboField{

	var $id = 'geo-point';
	
	var $fields = array(
		'latitude' => array(
		  'label' => 'Latitude',
		  'type' => 'text'
        ),
		'longitude' => array(
		  'label' => 'Longitude',
		  'type' => 'text'
        )
	);
	
}