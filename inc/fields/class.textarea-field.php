<?php

class TextareaField extends PostModernField{

	var $id = 'textarea';
	
	var $options = array(
		'rows' => 4,
		'cols' => 80
	);
	
	function get_column_value( $value ){
	   $max_length = 60;
	   if( strlen($value) > $max_length )
	       $value = substr($value, 0, $max_length -1);
        return $value;
	}
	
	function render_form($return=false){
		$field = &$this->config;
		
		if( !($field['id'] || $field['label']) ){ 
			$output = __('Cannot create field with neither an ID nor a label');
			if( $return ) return $output;
			
			echo $output;
			return;
		}
		// if no label supplied, create one from the id
		$label = ($field['label'])? $field['label'] : PostModern::humanize_identifier( $field['id'] );
		// get the value to output
		$value = $this->get();
		// grab the DOM name and id
		$dom_id = $this->post_type.'_'.$this->config['id'];
		$input_name = $this->get_input_name();
		// wrap helptext with DOM structure (because that structure should not be output otherwise)
		$helptext = (!empty($field['hint']))? '<div class="helptext">'.$field['hint'].'</div>':'';
		// set up validation stuff
		if( !$this->validate( trim($value) ) ){
          $valid_class = ' invalid ';
          $valid_message = '<div class="validation_error">'.$this->__validate_error.'</div>';
		} else {
		  $valid_class = '';
		  $valid_message = '';
		}
		// the list of classes to be output on the wrapper
		$classes = (!empty($field['class']))? $field['class']: '';
		$classes = $this->classes($classes).$valid_class;
		// the list of classes to be output on the input itself
		$input_classes = (!empty($field['input_class']))? $field['input_class']: '';
		$input_classes = $this->input_classes($input_classes).$valid_class;
				
		$output = '<div class="'.$classes.'">'.
		            $valid_message.
				 	'<label for="'.$dom_id.'">'.$label.'</label>'.
					$helptext.
				 	'<textarea class="'.$input_classes.'" name="'.$input_name.'" id="'.$dom_id.'" cols="'.$field['cols'].'" rows="'.$field['rows'].'">'.$value.'</textarea>'.
				 '</div><!-- #'.$dom_id.' -->';
				 
		if ( $return ) return $output;
		
		echo $output;
		
    }

}