<?php

class SelectField extends PostModernField{

	var $id = 'select';
	
	/* 
		additional config:
		
			options - the list of options to choose from
			multiple - whether or not to render as a multi-select
	
	*/
	var $options = array(
		'options' => array(),
		'multiple' => false,
		'size' => '5',
		'preprend' => '',
		'append' => '' 
	); 
	
	function admin_init(){
        wp_enqueue_style('chosen', plugins_url('post-modern/js/libs/chosen/chosen.css'), '0.95' );
        wp_enqueue_script(
            'chosen', 
            plugins_url('post-modern/js/libs/chosen/chosen.jquery.min.js'), 
            array('jquery'), // dependencies
            '0.95', // version
            true // footer?
        );
	}
	
	function get_column_value($value){
		return $this->config['options'][$value];
	}
	
	function render_form($return=false){
		$field = &$this->config;
		if( !($field['id'] || $field['label']) ){ 
            __('Cannot create field with neither an ID nor a label.<br>');
			if( $return ) return $output;
			
			echo $output;
			return;
		}
		
		// if no label supplied, create one from the id
		$label = (!empty($field['label']))? $field['label'] : PostModern::humanize_identifier( $field['id'] );
		// get the value to output
		$value = $this->get();
		// grab the DOM 
		$dom_id = $this->post_type.'_'.$this->config['id'];
		$input_name = $this->get_input_name();
		
		// grab the placeholder
		$placeholder = (!empty($field['placeholder']))? ' data-placeholder="'.esc_attr($field['placeholder']).'"' : '';
		// wrap helptext with DOM structure (because that structure should not be output otherwise)
		$helptext = (!empty($field['hint']))? '<div class="helptext">'.$field['hint'].'</div>':'';
		// get the value to display before the field
		$prepend = (!empty($field['prepend']))? '<span class="prepend">'.$field['prepend'].'</span>' : '';
		// get the value to display after the field
		$append = (!empty($field['append']))? '<span class="append">'.$field['append'].'</span>' : '';
		// the list of classes to be output on the wrapper
		$classes = (!empty($field['class']))? $field['class']: '';
		$classes = $this->classes( $classes);
		// the list of classes to be output on the input itself
		$input_classes = (!empty($field['input_class']))? $field['input_class']: '';
		$input_classes = $this->input_classes( $input_classes );
		
		// check for multiple
		$size = min( $field['size'], count($field['options']) );
		if( isset($field['multiple']) && $field['multiple'] ){
			$multiple = 'multiple="multiple" size="'.$size.'"';
			$input_name .= '[]';
		} else {
			$multiple = '';
		}
		// setup the options output with an empty option
		$options = '<option value=""></option>';
		if( !empty($field['options']) ){ 
    		foreach( $field['options'] as $option_value => $option_label ){
    			// check if it is an option group
    			if( is_array($option_label) ){
    				$options .= '<optgroup label="'.$option_value.'">';
    				foreach( $option_label as $grp_opt_val => $grp_opt_label )
    					$options .= $this->render_option($grp_opt_val,$grp_opt_label,$value);
    				$options .= '</optgroup>';
    			} else {
    				$options .= $this->render_option($option_value,$option_label,$value);
    			}
    		}
        }		
		$output = '<div class="'.$classes.'">'.
				 	'<label for="'.$input_name.'">'.$label.'</label>'.
				 	$prepend.
				 	'<select class="'.$input_classes.'" name="'.$input_name.'" id="'.$dom_id.'"'.$multiple.$placeholder.'>'.
				 		$options.
				 	'</select>'.
				 	$append.
				 	$helptext.
				  '</div><!-- #'.$dom_id.' -->';
				 
		if ( $return ) return $output;
				
		echo $output;
		
    }
    
    /**
     * Returns the output for a single option.
     * 
     * @param string $value the value for this option
     * @param string $label the label for this option
     * @param mixed $sel_val the selected value of this field (string or array)
     * @return HTML formatted option
     */
    function render_option($value,$label,$sel_val){
		// determine if selected, even if multiple
		if( is_array($sel_val) ){
			$selected = ( in_array($value, $sel_val) )? ' selected="selected"':'';
		} else {
			$selected = ($sel_val == $value)? ' selected="selected"':'';
		}
    	return '<option value="'.esc_attr($value).'"'.$selected.'>'.$label.'</option>';
    }

}