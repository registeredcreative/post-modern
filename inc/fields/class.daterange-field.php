<?php
/**
 * Date Range Combo-Field  
 *
 * A combo field for date ranges
 * 
 *
 * @package PostModern
 * @subpackage AddressField
 * @since 0.0.1
 */
class DateRangeField extends PostModernComboField{

	var $id = 'date_range';
	
	var $fields = array(
		'start' => array(
		  'label' => 'Start',
		  'type' => 'date'
        ),
		'end' => array(
		  'label' => 'End',
		  'type' => 'date'
        )
	);
	
	var $helptext_before = false;
	
    /**
     * The options for this field combo.   
     * @var array
     */		
	var $options = array(
	);
	
	function pre_save(){
		
	}
	
	function init(){
		// TODO: add support for start_before, start_after, end_before and end_after validators
		if( !empty($this->config['validate']) ) {
            $this->fields['start']['validate'] = $this->config['validate'];
    		$this->fields['end']['validate'] = $this->config['validate'];
        }
		if( !empty($this->config['format']) ){
            $this->fields['start']['format'] = $this->config['format'];
            $this->fields['end']['format'] = $this->config['format'];
        }
	}
	
    /**
     * Determines whether or not this field is valid. Can be overriden by a callback in the field config.
     *
     * @param mixed $value the value to validate. Optional, will default to the current value of the field.
     */
	function validate($value=null){
		if( $value == null ) $value = $this->get();
		
		if( !empty($this->config['validate_callback']) ){
			if( is_callable($this->config['validate_callback']) ){
				return call_user_func( $this->config['validate_callback'], $value);// return the return value from the user function
			} else {
				// TODO: log error
				trigger_error('Unable to call user function \''.$this->config['validate_callback'].'\' to validate \''.$this->config['id'].'\'', E_USER_WARNING);
			}
		}
		$valid = true;
        foreach( $this->__fields as $id => &$field ){
            $val = !empty($value[$id])?$value[$id]:'';
            if( !$valid = $field->is_valid($val) ){
            	$this->__validate_error = $field->__validate_error;
            	break;
            }
        }
        // if it is still valid, check that start is before finish
        if( $valid && !empty($value['start']) && !empty($value['end'])){
        	$valid = strtotime($value['start']) <= strtotime($value['end']);
        	if( !$valid ) $this->__validate_error = 'End date cannot be before start date.';
        }
        return $valid;
	}
}