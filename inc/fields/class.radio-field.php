<?php

class RadioField extends PostModernField{

	var $id = 'radio';
	
	var $defaults = array(
		'options' => array(),
		'default' => null
	); 
	
	function get_column_value($value){
		return $this->config['options'][$value];
	}
	
	function render_form($return=false){
		$field = &$this->config;
		if( !($field['id'] || $field['label']) ){ 
			$output = __('Cannot create field with neither an ID nor a label');
			if( $return ) return $output;
			
			echo $output;
			return;
		}
		
		$field = array_merge( $this->defaults, $field );
		
		// if no label supplied, create one from the id
		$label = (!empty($field['label']))? $field['label'] : PostModern::humanize_identifier( $field['id'] );
		// get the value to output
		$value = $this->get();
		// grab the DOM name and id
		$dom_id = $this->post_type.'_'.$this->config['id'];
		$input_name = $this->get_input_name();
		// wrap helptext with DOM structure (because that structure should not be output otherwise)
		$helptext = (!empty($field['hint']))? '<div class="helptext">'.$field['hint'].'</div>':'';
		// the list of classes to be output on the wrapper
		$classes = (!empty($field['class']))? $field['class']: '';
		$classes = $this->classes( $classes);
		// the list of classes to be output on the input itself
		$input_classes = (!empty($field['input_class']))? $field['input_class']: '';
		$input_classes = $this->input_classes( $input_classes );
		// setup the options output
		$options = '';
		$i = 0;
		foreach( $field['options'] as $option_value => $option_display ){
			$checked = ($value == $option_value)? ' checked':'';
			$options .= '<label class="wrapping_label radio_label">'.
							'<input class="'.$input_classes.'" name="'.$input_name.'" id="'.$dom_id.'" type="radio" value="'.$option_value.'"'.$checked.'>'.
							$option_display.
						'</label>';
			$i++;
		}
		
		$output = '<div class="'.$classes.'">'.
				 	'<label>'.$label.'</label>'.
					$helptext.
					'<div class="radio_options">'.
						$options.
					'</div><!-- .radio-options -->'.
				  '</div><!-- #'.$dom_id.' -->';
				 
		if ( $return ) return $output;
		
		echo $output;
		
    }

}