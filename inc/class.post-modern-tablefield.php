<?php
/**
 * Base Class for PostModern Table-Field Objects 
 *
 * Creates an abstract base and implements common functionality of PostModern table-fields
 * 
 *
 * @package PostModern
 * @subpackage PostModernTableField
 * @since 0.0.1
 */
abstract class PostModernTableField implements PostModernFieldInterface{
    /**
     * The unique identifier for this field. Referred to as 'type' elsewhere in the system. 
     * @var string
     */
	var $id = null;
    /**
     * An alias of id. 
     * @var string
     */	
	var $type = null;
    /**
     * The post_type of the parent object.   
     * @var string
     */		
	var $post_type = null;
    /**
     * Associative array that describes the configuration of this field.
     * @var array
     */
	var $config = array(
		'id' => '',
		'label' => '',
		'hint' => '',
		'fields' => array(), 
		'class' => '',
		'input_class' => '',
		'validate' => '',
		'pre_save_callback' => '',
		'save_callback' => '',
		'sanitize_callback' => '',
		'validate_callback' => '',
		'value' => '',
		'min_rows' => '',
		'max_rows' => '',
		'save_invalid' => true
	);
    /**
     * Associative array or pre-configured fields.
     * @var array
     */
	var $fields = array();
    /**
     * the additional options a child field might have
     * @var string
     */	
	var $options = array();	
	/**
	 * display helptext before displaying the fields
	 * var bool
	 */
    var $helptext_before = true;
    /**
     * the error to display when this field is not valid.   
     * @var string
     */		
	var $__validate_error = null;	
    /**
     * the list of post modern field objects
     * @var array
     */
    var $__fields = array();
    /**
     * Construct the object.
     *
     * @param string $post_type Post type of the  
     * @param array $config Associative array that describes the configuration of the object 
     */
	function __construct($post_type, $config){
		$this->post_type = $post_type;
		$this->type =& $this->id;
		
		if( is_array($config) )
            $this->config = array_merge( $this->config, $this->options, $config);
			
		$this->init();
		$this->check_id();
		
        foreach( $this->fields as $f_id => $field ){
            $field['id'] = $config['id'].'_'.$f_id;
            $this->__fields[$f_id] = $this->new_field( $field['type'], $field ); 
        }
	}	
    /**
     * Callback that allows subclasses to do work when the object is initialized.
     *
     */	
	function init(){}
    /**
     * Ensure that there is an id by creating one from the 'label' if necessary
     *
     */
	function check_id(){
		if( !$this->config['id'] ){ 
			// if there is a label, use it to create an id
			if( $this->config['label'] )
				$this->config['id'] = PostModern::sanitize_identifier( $this->config['label'] );
		}
		$this->config['id'] = trim($this->config['id']);
	}
    /**
     * Get the value of this field
     *
     * @param int $post_id The id of the post to retieve the value. Optional, will default to the current post.
     * @return mixed the value of the current field object.
     */
    function get($post_ID=null){
		if( !$post_ID ){
			global $post_ID;
		}
		// if we haven't already retrieved them, do it now.
		if( !isset($this->config['value']) ){
            $this->config['value'] = array();
        }
		// if we haven't already retrieved it, do it now. Only going back to the database once throughout the request.
		if( empty($this->config['value']) ){
			$value = array_shift(get_post_meta( $post_ID, $this->config['id'] ) );
			if( !is_array($value) ) $value = unserialize($value);
			$this->config['value'] =  $value;
			$this->post_get();
		}
		return $this->config['value'];
    }
    /**
     * Retrieves the value for this field from $_POST
     *
     * @return array The value of this field  
     */    
    function get_posted_value(){
        $values = array();
    	if( !empty($_POST[$this->post_type][$this->config['id']]) ){
        	$values = $_POST[$this->post_type][$this->config['id']];
        }
    	// TODO: sanitize the input before passing it all around kingdom come
    	return $values;
    } 
    /**
     * Extracts the value for this field from the results of get_post_custom.
     *
     * @param array $post_custom the results of get_post_custom to parse
     * @return mixed The value of this field  
     */
    function extract($post_custom){
        if( isset($post_custom[ $this->config['id'] ]) ){
            $value = $post_custom[ $this->config['id'] ];
        } else {
        	$value = $post_custom;
        }
        
        if( empty($value) ) return array();
        // all post meta retrieved from get_post_custom are arrays
        if( count($value) == 1 ) $value = $value[0];
		
		if( !is_array($value) ) $value = unserialize($value);
		return $value;
    }
    /**
     * Post-get callback allows subclasses to do work after the value is loaded from the DB without the need to  
     * override the get or extract functions. Only called after DB retrieval.
     *
     */	
	function post_get(){}
    /**
     * Returns a short representative value appropriate for showing in the table in the edit-post_type screen.
     *
     * @param int $post_id The id of the post to retieve the value. Optional, will default to the current post.
     * @return mixed the value of the current field object.
     */
    function get_column_value($value){
        return __('This field is a table&#8230;');
    }
    /**
     * Set the value of this field
     *
     * @param array $value The value to associate with this field  
     */
    function set($value){
        $this->config['value'] = $value;
    }
    /**
     * Pre-save callback allows subclasses to do work before the save takes place without the need to override 
     * the save function. Called before sanitize.
     *
     */	
	function pre_save(){}
    /**
     * Saves the field from the POST data. 
     *
     * @param int $post_id The id of the post to retieve the value. Optional, will default to the current post.
     * @return bool true on success and false on failure 
     */	
    function save($post_ID=null){
		if( !$post_ID ){
			global $post_ID;
		}
        $this->config['value'] = $this->get_posted_value();
		$this->config['value'] = $this->sanitize( $this->config['value'] );
		$this->pre_save();
        
		if( !empty($this->config['save_callback']) ){
			if( is_callable($this->config['save_callback']) ){
				return call_user_func( $this->config['save_callback'], $value);// return the return value from the user function
			} else {
				trigger_error('Unable to call user function \''.$this->config['save_callback'].'\' to save \''.$this->config['id'].'\'', E_USER_WARNING);
			}
		}
		
		$success = false;
		$empty = true;
		foreach( $this->config['value'] as $r ){
			foreach( $r as $c ){
				if( !empty($c) ){
					$empty = false;
					break;
				}
			}
			if( !$empty ) break;
		}
		// if empty, delete it
		if( $empty ){
			$success = delete_post_meta( $post_ID, $this->config['id'] );
		} else {
            $success = $this->save_field($post_ID, $this->config['id'], $this->config['value']);
		}
        
		if( $success ) return true;

		return false;
    }
	/**
     * Store the field value in the database.
     *
     * @param int $post_id The id of the post to associate this field
     * @param string $field The meta_key for the field to save
     * @param mixed $value Array or string value to save
     * @return bool Whether or not the save was successful. In the case of arrays, will return whether all were successful
     */	
	function save_field($post_id, $field, $value){
		//if( is_array($value) ) $value = serialize($value);
		$success = update_post_meta( 
			$post_id, 
			$field, 
			$value
		);
		if( $success === false ){
			//trigger_error('Unable to save single field, \''.$field.'\' with value \''.$value.'\'', E_USER_NOTICE);
		}
		return $success;
	}

    /**
     * Returns a sanitized value appropriate for this field. Can be overriden by a callback in the field config.
     *
     * @param mixed $value the value to sanitize. Optional, will default to the current value of the field.
     * @return mixed the sanitized value.
     */
	function sanitize($value){
		if( !empty($this->config['sanitize_callback']) ){
			if( is_callable($this->config['sanitize_callback']) ){
				return call_user_func( $this->config['sanitize_callback'], $value);// return the return value from the user function
			} else {
				trigger_error('Unable to call user function \''.$this->config['sanitize_callback'].'\' to sanitize \''.$this->config['id'].'\'', E_USER_WARNING);
			}
		}
		$clean = array();
		$num_rows = count($value);
		$value = array_values($value);
        for( $i = 0; $i < $num_rows; $i++ ){
            foreach( $this->__fields as $id => &$field ){
                $clean[$i][$id] = $field->clean( $value[$i][$id] );
            }
        }
        return $clean;
	}
    /**
     * Determines whether or not this field is valid. Can be overriden by a callback in the field config.
     *
     * @param mixed $value the value to validate. Optional, will default to the current value of the field.
     */
	function validate($value=null){
		if( $value == null ) $value = $this->get();
		if( !empty($this->config['validate_callback']) ){
			if( is_callable($this->config['validate_callback']) ){
				return call_user_func( $this->config['validate_callback'], $value);// return the return value from the user function
			} else {
				trigger_error('Unable to call user function \''.$this->config['validate_callback'].'\' to validate \''.$this->config['id'].'\'', E_USER_WARNING);
			}
		}
		$valid = true;
		$num_rows = count($this->config['value']);
        for( $i = 0; $i < $num_rows; $i++ ){
            foreach( $this->__fields as $id => &$field ){
                $valid = $field->is_valid( $value[$i][$id] );
            }
        }
        return $valid;
	}
    /**
     * get the form input name
     *
     */
	function get_input_name(){
		$names = array();
		foreach($this->fields as $id => $field ){
			$names[$id] = $this->post_type.'['.$this->config['id'].'][%d]['.$id.']';
		}
		return $names;
	}
    /**
     * get the DOM ids
     *
     */
	function get_input_id(){
		$ids = array();
		foreach($this->fields as $key => $label ){
			$ids[$key] = $this->post_type.'_'.$this->config['id'].'_%d_'.$key;
		}
		return $ids;
	}
    /**
     * Returns a space separated list of CSS classes for the field wrapper (suitable for setting the class variable).
     *
     * @param string $class additional class(es) to add to the string
     * @return string the list of classes.
     */	
	function classes($class=null){
		global $post; 
		$post_status = ( $post )? $post->post_status : 'auto-draft';
		return implode( ' ', array(
			'form_table_field',
			$this->id.'_field',
			$this->post_type.'_meta_field',
			$post_status,
			sanitize_html_class( $class )
		));
	}
    /**
     * Prints the HTML code necessary to edit this field inside of a form object.
     *
     * @param bool $return whether or not to return the output instead of printing it
     * @return string 
     */
    function render_form($return=false){
		$field = &$this->config;
		
		if( !($field['id'] || $field['label']) ){ 
			$output = __('Cannot create field with neither an ID nor a label.<br>');
			if( $return ) return $output;
			
			echo $output;
			return;
		}
		// if no label supplied, create one from the id
		$label = (!empty($field['label']))? $field['label'] : PostModern::humanize_identifier( $field['id'] );
		// get the values
		$values = $this->get();
		// wrap helptext with DOM structure (because that structure should not be output otherwise)
		$ht_class = ($this->helptext_before)? 'before' : 'after';
		$helptext = (!empty($field['hint']))? '<div class="helptext '.$ht_class.'">'.$field['hint'].'</div>' : '';
		// set up validation stuff
		if( !$this->validate($values) ){
          $valid_class = ' invalid';
          $valid_message = '<div class="validation_error">'.$this->__validate_error.'</div>';
		} else {
		  $valid_class = '';
		  $valid_message = '';
		}
		// the list of classes to be output on the wrapper
		$classes = (!empty($field['class']))? $field['class']: '';
		$classes = $this->classes($classes).$valid_class;
        
        $fields_output = '<table border="0" cellpadding="0" cellspacing="0">';
        $num_rows = count($values);
        $input_names = $this->get_input_name();
        $input_ids = $this->get_input_id();
        $num_rows = max( array(1, $num_rows, $this->config['min_rows']) );
        for( $i = 0; $i < $num_rows; $i++ ){
            $clone = ($i > 0)?'clone':'';
            $fields_output .= '<tr class="clonable '.$clone.'">';
            foreach( $this->__fields as $f_id => $f ){
                $f_val = ( !empty($values[$i][$f_id]) )? $values[$i][$f_id]:''; 
                $f->set($f_val);
                $f->input_name = sprintf($input_names[$f_id], $i);
                $f->input_id = sprintf($input_ids[$f_id], $i);
                $fields_output .= '<td>'.$f->render_form(true).'</td>';
            }
            $fields_output .= '</tr>';
        }    				
        $fields_output .= "</table>";
		$output = '<div class="'.$classes.'">'.
		            $valid_message.
				 	'<label>'.$label.'</label>'.
				 	($this->helptext_before?$helptext:'').
				 	'<div class="table_field_wrapper">'.
				 	$fields_output.
				 	'</div>'.
				 	(!$this->helptext_before?$helptext:'').
				  '</div>';
				 
		if ( $return ) return $output;
		
		echo $output;
		
    }
    /**
     * Instantiates the requested field type (child of PostModernField). Falls back to
     * TextField if class not found.  
     * 
     * @param string $type The field type idenifier (text, radio, checkbox, select, textarea, etc.)
     * @param array $config An associative array containing the field configuration data
     *   
     */
 	function new_field( $type, $config ){
        // convert the identifier
 		$class_name = PostModern::camelize_identifier($type);
 		if( class_exists( $class_name.'Field' ) ) 
            $class_name .= 'Field'; 
 		else{
			// fallback to a textfield
			$class_name = 'TextField';
 		}
		return new $class_name($this->post_type,$config);
 	}
}