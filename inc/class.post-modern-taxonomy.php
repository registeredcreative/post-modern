<?php 
/**
 * A wrapper around WordPress' custom taxonomy functionality.  
 *
 * Simplifies the handling of custom taxonomies translating a straightforward 
 * configuration array into the callbacks on actions and filters that power WordPress 
 * 
 *
 * @package PostModern
 * @subpackage PostModernTaxonomy
 * @since 0.0.1
 */
class PostModernTaxonomy extends PostModernObject{
    /**
     * singular name, display style. 
     * @var string
     */
    var $name = null;
    /**
     * plural name, display style 
     * @var string
     */
    var $plural_name = null;
    /**
     * the post_type identifier, slug style, to supply to wordpress. if null, automatically built from $name
     * @var string
     */
    var $taxonomy = null;
    /**
     * array of object types to associate with this taxonomy
     * @var array
     */
    var $object_type = array();
    /**
     * taken right from register_taxonomy. @link http://codex.wordpress.org/Function_Reference/register_taxonomy
     * @var array
     */
    var $args = array();

    /**
     * Initialize the object. Object must have config by this point. Called on action 'init'. 
     *     
     */
    function init(){
        // if it hasn't been setup, we need to quit
        if ( !$this->__configured ) return;
        
        $this->setup_identifier();
        $this->setup_preferences();
        $this->setup_labels();
        $this->setup_help();
        $this->register();        

    }

    /**
     *  Handles the matching up of var's id and taxonomy. Creates one from name if necessary. 
     *     
     */
    function setup_identifier(){
        // if ID supplied, set the taxonomy
        if( $this->id ) $this->taxonomy = $this->id;
        // if taxonomy supplied, set the id
        else if( $this->taxonomy ) $this->id = $this->id;
        // if neither specified, build it from the name
        else $this->taxonomy = $this->id = PostModern::sanitize_identifier( $this->name );
    }
    
    /**
     *  Sets up behaviors of handling taxonomies that are preferences. 
     *     
     */
    function setup_preferences(){
        
        // preference: automatically handles the update_count_callback for you
        // (for more: see http://codex.wordpress.org/Function_Reference/register_taxonomy#Example )
        if( empty($this->args['hierarchical']) || !$this->args['hierarchical'] ){
            if( !isset($this->args['update_count_callback']) )
                $this->args['update_count_callback'] = '_update_post_term_count';
        }
    }
    
    /**
     * Handles writing default labels based on the name of the object. 
     * Customizes WordPress' generic default labels while still allowing the 
     * overriding any (or all) specific message. 
     *     
     */
    function setup_labels(){
        // recreate WordPress' default labels based around the name of the object
        $default_labels = array(
            'name' => _x( $this->plural_name, $this->plural_name ),
            'singular_name' => _x( $this->name, $this->name ),
            'search_items' =>  __( 'Search '.$this->plural_name ),
            'popular_items' => __( 'Popular '.$this->plural_name ),
            'all_items' => __( 'All '.$this->plural_name ),
            'parent_item' => __( 'Parent '.$this->name ),
            'parent_item_colon' => __( 'Parent '.$this->name.':' ),
            'edit_item' => __( 'Edit '.$this->name ), 
            'update_item' => __( 'Update '.$this->name ),
            'add_new_item' => __( 'Add New '.$this->name ),
            'new_item_name' => __( 'New '.$this->name.' Name' ),
            'separate_items_with_commas' => __( 'Separate '.strtolower($this->plural_name).' with commas' ),
            'add_or_remove_items' => __( 'Add or remove '.strtolower($this->plural_name) ),
            'choose_from_most_used' => __( 'Choose from the most used '.strtolower($this->plural_name) ),
            'menu_name' => __( $this->plural_name ),
        ); 
        
        // for category style, remove tag labels 
        if( !empty($this->args['hierarchical']) && $this->args['hierarchical'] ){
            $default_labels = array_merge(
                $default_labels,
                array(
                    'popular_items' => null,
                    'separate_items_with_commas' => null,
                    'add_or_remove_items' => null,
                    'choose_from_most_used' => null
                )
            );                
        } else { // for tag style, remove parent references
            $default_labels = array_merge(
                $default_labels,
                array(
                    'parent_item' => null,
                    'parent_item_colon' => null
                )
            );
        }
        
        // look for a conforming labels array to merge in
        if( isset($this->args['labels']) && is_array( $this->args['labels'])){
            $this->args['labels'] = array_merge($default_labels, $this->args['labels']);
        } else { // otherwise just use the defaults
            $this->args['labels'] = $default_labels;
        }
    }
    
    /**
     * Registers the taxonomy.
     *     
     */ 
    function register(){
        if ( $this->__registered ) return false;
        register_taxonomy($this->taxonomy, $this->object_type, $this->args);
        $this->__registered = true;
    }
}