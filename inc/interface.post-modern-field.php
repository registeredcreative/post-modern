<?php

interface PostModernFieldInterface{

    /**
     * Callback that allows subclasses to do work when the object is initialized.
     *
     */	
	function init();
    /**
     * Get the value of this field
     *
     * @param int $post_ID The id of the post to retieve the value. Optional, will default to the current post.
     * @return mixed the value of the current field object.
     */
    function get($post_ID=null);
    /**
     * Retrieves the value for this vield from $_POST
     *
     * @return mixed The value of this field  
     */    
    function get_posted_value();
    /**
     * Extracts the value for this vield from the results of get_post_custom.
     *
     * @param array $post_custom the results of get_post_custom to parse
     * @return mixed The value of this field  
     */
    function extract($post_custom);
    /**
     * Returns a short representative value appropriate for showing in the table in the edit-post_type screen.
     *
     * @param int $post_id The id of the post to retieve the value. Optional, will default to the current post.
     * @return mixed the value of the current field object.
     */
    function get_column_value($value);
    /**
     * Set the value of this field
     *
     * @param mixed $value The value to associate with this field  
     */
    function set($value);
    /**
     * Pre-save callback allows subclasses to do work before the save takes place without the need to override 
     * the save function. Called before sanitize.
     *
     */	
	function pre_save();
    /**
     * Saves the field from the POST data. 
     *
     * @param int $post_id The id of the post to retieve the value. Optional, will default to the current post.
     * @return bool true on success and false on failure 
     */	
    function save();
    /**
     * Returns a sanitized value appropriate for this field. Can be overriden by a callback in the field config.
     *
     * @param mixed $value the value to sanitize. Optional, will default to the current value of the field.
     * @return mixed the sanitized value.
     */
	function sanitize($value);
    /**
     * Determines whether or not this field is valid. Can be overriden by a callback in the field config.
     *
     * @param mixed $value the value to validate. Optional, will default to the current value of the field.
     */
	function validate($value=null);
    /**
     * get the form input name
     *
     */
	function get_input_name();
    /**
     * Returns a space separated list of CSS classes for the field wrapper (suitable for setting the class variable).
     *
     * @param string $class additional class(es) to add to the string
     * @return string the list of classes.
     */	
	function classes($class=null);
    /**
     * Prints the HTML code necessary to edit this field inside of a form object.
     *
     * @param bool $return whether or not to return the output instead of printing it
     * @return string 
     */
	function render_form($return=false);	
	
}