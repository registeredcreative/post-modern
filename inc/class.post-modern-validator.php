<?php
/**
 * Base Class for PostModern Validator Objects 
 *
 * Creates an abstract base and implements common functionality of PostModern Validators (email, url, date, etc.)
 * 
 *
 * @package PostModern
 * @subpackage PostModernValidator
 * @since 0.0.1
 */
class PostModernValidator{
	

	// setup user validators
	function register_validator($validator){

	}
	
	static function not_empty( $value ){
		$value = trim($value);
		return !empty($value);
	}

	static function min_length( $value, $length ){
		$value = trim( $value );
		
		return strlen( $value ) >= $length;
	}
	
	static function max_length( $value, $length ){
		return strlen( $value ) <= $length;
	}
	
	static function email( $email ){
		return is_email( $email );
	}
	
	static function url( $url ){
		// http://www.php.net/manual/en/function.filter-var.php#104160
		
		$result = filter_var($url, FILTER_VALIDATE_URL);
		if ($result) return $result;
		// Check if it has unicode chars.
		$l = mb_strlen($url);
		if ($l !== strlen($url)) {
		    // Replace wide chars by �X�.
		    $s = str_repeat(' ', $l);
		    for ($i = 0; $i < $l; ++$i) {
		        $ch = mb_substr($url, $i, 1);
		        $s [$i] = strlen($ch) > 1 ? 'X' : $ch;
		    }
		    // Re-check now
		    return filter_var($s, FILTER_VALIDATE_URL);
		}
		return false;
	}
	
	static function int( $num ){
		return is_numeric( $num );
	}
	
	static function date($date){
		return strtotime($date) == false;
	}

	static function after($date, $after){
	   $after = strtotime($after);
       $date = strtotime($date);
	   
	   return $date > $after;
	}	

	static function before($date, $before){
	   $before = strtotime($before);
       $date = strtotime($date);
       
	   return $date < $before;
	}
	
	static function between($date, $args){
	   list($after,$before) = $args;
	   if( empty($after) || empty($before) ){
	       // TODO: log error and return true
	       trigger_error('Both after and before values must be supplied for between validator');
	       return true;
	   }
	   $after = strtotime($after);
	   $before = strtotime($before);
       $date = strtotime($date);
       
	   return $date > $after && $date < $before;
	}

}