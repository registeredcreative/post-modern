<?php

add_action('wp_ajax_pomo_add_post','pomo_quick_add_post');

function pomo_quick_add_post(){
	check_ajax_referer(WP_PLUGIN_DIR.'pomo-ajax-quick-add-post','quick_add_nonce'); 
    // get the title
    if( !empty($_POST['post_title']) ){
	    $post['post_title'] = stripslashes($_POST['post_title']);
	    $post['post_type'] = !empty($_POST['post_type'])? $_POST['post_type'] : 'post';
	    $post['post_status'] = !empty($_POST['post_status'])? $_POST['post_status'] : 'publish';
 
	    $post['ID'] = wp_insert_post($post); 
	    // response output
	    header( "Content-Type: application/json" );
	    echo json_encode( $post );
	} else{
		echo '-1';
	} 
    // IMPORTANT: don't forget to "exit"
    exit;
}