<?php

class PostModern{

    // An array of instantiated objects 
    var $objects = array(); 
    
    var $output_styles = true;
    var $output_scripts = true;
    
    

    function __construct($options=null,$post_types=null, $taxonomies=null){
        if( is_array( $post_types ) ){
            $this->register_post_types($post_types);
        }
        if( is_array( $taxonomies ) ){
            $this->register_post_types($taxonomies);
        }
        
        add_action('admin_init',array(&$this,'admin_init'), 9);
    }
    
    function register_post_type( $post_type ){
       	$this->register_object( 'PostModernType', $post_type );
    }
    
    function register_post_types( $post_types ){
       	foreach( $post_types as $post_type ){
	       	if ( is_array($post_type) )
		       	$this->register_object( 'PostModernType', $post_type );
	    }
    }
    
    function register_taxonomy( $taxonomy ){
       	$this->register_object( 'PostModernTaxonomy', $taxonomy );
    }
    
    function register_taxonomies( $taxonomies ){
       	foreach( $taxonomies as $taxonomy ){
       		if( is_array($taxonomy) )
		       	$this->register_object( 'PostModernTaxonomy', $taxonomy );    	
		}
    }
        
    function register_object($class, $config){
        $this->objects[$this->sanitize_identifier($config['name'])] = new $class($config);
    }
    
    function admin_init(){
    	
    	if( $this->output_styles ){
        	wp_enqueue_style('post-modern', plugins_url('post-modern/css/post-modern.css') );
        }
        	
    	if( $this->output_scripts ){
            wp_enqueue_script(
                'post-modern', 
                plugins_url('post-modern/js/post-modern.js'), 
                array('jquery'),//,'jquery-ui-date-picker','chosen'), // dependencies
                '0.1', // version
                true // footer?
            );
        }
    }
    
    /**
     * Validation
     */
    function validate($function, $value, $args=null ){
        if( !$args ) $args = array();
        
        if( is_callable( 'PostModernValidator::'.$function ) ){
            return call_user_func('PostModernValidator::'.$function, $value, $args );
        }
        trigger_error('Unrecognized validator \''.$function.'\'', E_USER_WARNING);
        return true;
    }
    
    // utility function to convert display style names to post_type and taxonomy id's
    function sanitize_identifier( $name ){
        return substr(
            str_replace( // replace dashes with underscores in the sanitized return value
                '-',
                '_',
                strtolower( // lowercase the sanitized return value 
                    sanitize_title_with_dashes( $name )
                )
            ),
            0, // start at the beginning 
            19 // wordpress has a 20 char limit on post_type names
        );
    }
    
    // utility function to convert a slug-like id to a display style name
    function humanize_identifier( $id ){
    	return ucwords( str_replace('_',' ',$id) );
    }
    
    // utility function to convert a slug-like id to a camel case string
    static function camelize_identifier( $id ){
    	// turn it into words by replacing underscores with spaces
    	// uppercase the words
    	// strip the spaces
    	// trim it for good measure
    	return trim( str_replace( ' ', '', ucwords( str_replace(array('_','-'),' ', $id) ) ) );
    }

}
