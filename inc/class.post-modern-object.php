<?php
/**
 * Base Class for PostModern Registered Objects 
 *
 * Creates an abstract base and implements common functionality of registered objects (Post Types, Taxonomies).
 * 
 *
 * @package PostModern
 * @subpackage PostModernObject
 * @since 0.0.1
 */
abstract class PostModernObject{
    
    // The display style name of the object
    var $name = null;
    // The slug style identifier of the object as in the post_type 
    //   that would be passed to register_post_type
    var $id = null;
    
    // The file path of the template to render as contextual help on the post_type screen (admin single edit)
	var $help = null;
    // The file path of the template to render as contextual help on the edi-tpost_type screen (admin list view)
	var $edit_help = null;
	// admin notices, each element should conform to array( $notice_class, $message )
	// where $notice_class is either error, updated, or some user defined class for styling messages
    var $__notices = array();
	
    /**
     * keep track of whether this post type has already been registered
     * @var bool
     */
    var $__registered = false;
	
	// Called on WordPress action 'init'
    abstract function init();
    // Handles registering the object when ready
	abstract function register();
	// Handles creating an identifier when not supplied 
	abstract function setup_identifier();

    /**
     * Construct the object.
     *
     * @param array $config Associative array that describes the configuration of the object 
     */
    function __construct($config){
        $this->load_config( $config );
        add_action( 'init', array( &$this, 'init' ) );
        
        add_action('admin_init', array(&$this, 'admin_init'));
    }
    
    /**
     *  Loads the supplied object configuration into the object.
     *
     *  @param array $config Associative array that describes the configuration of the object     
     */
    function load_config($config){
		if( !is_array( $config ) ) return;
		
		foreach( $config as $var => $value ){
			$this->$var = $value;
		}
		$this->__configured = true;
    }
    
    /**
     * Admin init callback to be used by children
     */
    function admin_init(){
    
    }
    
    /**
     *  Adds a admin notice to be rendered.
     *
     *  @param string $class The class of notice, either updated, error or user defined     
     *  @param string $message The message to display. Can contain HTML formatting.     
     */
    function add_admin_notice($class,$message){
        if( !count($this->__notices) )
            add_action('admin_notices', array( &$this, 'render_admin_notices'));
        $this->__notices[] = array($class,$message);
    }
    
    /**
     * Renders admin notices
     *
     */
    function render_admin_notices(){
        foreach( $this->__notices as $notice ){
            list($class,$message) = $notice;
            echo '<div class="'.$class.'"><p>'.$message.'</p></div>';
        }
    }

    /**
     *  Set up the help callback. 
     *     
     */
    function setup_help(){
        if( $this->help || $this->edit_help ) {
            add_action('admin_head', array(&$this, 'render_contenxtual_help'), 10, 3 );
        }
    }

    /**
     *  Callback to handle loading the help template files. Called on action 'admin_head'. 
     *     
     */
    function render_contenxtual_help(){
        $tabs = null;
        $screen = get_current_screen();
        // first, figure out what screen we are on and get our tabs
        switch( $screen->id ){
            case $this->id:
                if( $this->help ) $tabs = $this->help;
                break;
            case 'edit-'.$this->id:
                if( $this->edit_help ) $tabs = $this->edit_help;    
                break;
            default:
                break;
        }
        // if we have any tabs, let's deal with them
        if( $tabs ){
            // first, look to see if it is a single string
            if( !is_array( $tabs ) ){
                // convert it to a "tab" using WordPress compatibility strategy
                $tabs = array(
                    array(
                        'id' => $this->id.'_overview',
                        'title' => 'Overview',
                        'content' => $tabs
                    )  
                );
            }
            // process each tab
            foreach( $tabs as $tab){
                // only create tabs that we have content for
                if( !empty($tab['content']) ){
                    // if the 'content' is a file path, grab the contents
                    if( is_file($tab['content']) ){
                        ob_start();
                        include($tab['content']);
                        $content = ob_get_contents();
                        ob_end_clean(); 
                        $tab['content'] = $content;
                    } 
                    // finally, add the tab using the screen API
                    if( 'sidebar' == $tab['id'] ){
                        $screen->set_help_sidebar( $tab['content'] );
                    } else {
                        $screen->add_help_tab( $tab );
                    }
                }
            }
        }        
        return;
    }

}
