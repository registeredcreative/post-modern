<?php
/*
Plugin Name: PostModern
Plugin URI: 
Description: A simple, modern approach to creating and managing custom post types. 
Author: Mark Branly
Version: 0.0.1
Author URI: 
*/


/* Example Usage:
-------


if( class_exists('PostModern') ): // set up classes

$Movie = array( 
    'name' => 'Movie', 
    'plural_name' => 'Movies', 
    
    'args' => array(
        'public' => true,
        'show_in_menu' => true
    ), 
    
    'post_meta' => array(
        'director' => array(
            'label' => 'Director',
            'type' => 'text',
            'hint' => 'Enter the name of the director of the movie',
            'add_column' => true
        ),
        'distributor' => array(
            'label' => 'Distributor',
            'type' => 'text',
            'hint' => 'Enter the name of the distributor of the movie'          
        )  
    ),
    // layout your posts listing page with this simple array
    'index_columns' => array(
    	'cb' => '<input type="checkbox" />',
    	'title' => __('Title'),
    	'genre' => __('Genre'),
    	'director' => __('Director'),
    	'distributor' => __('Distributor'),
    	'date' => __('Date')
    )
);


$Genre = array(
	'name' => 'Genre',
	'plural_name' => 'Genres',
	'object_type' => array('movie'),
	'args' => array(
        'public' => true,
        'show_in_menu' => true
    )
);

$PoMo = new PostModern( );
$PoMo->register_post_type($Movie);
$PoMo->register_taxonomy($Genre);

endif;

-------
*/


if( !class_exists('PostModern') ){ // check for class before instantiating 

    $pomo_dir = dirname(__FILE__);
    require_once($pomo_dir.'/inc/ajax.php');
    require_once($pomo_dir.'/inc/class.post-modern.php');
    require_once($pomo_dir.'/inc/class.post-modern-object.php');
    require_once($pomo_dir.'/inc/class.post-modern-type.php');
    require_once($pomo_dir.'/inc/class.post-modern-taxonomy.php');
    require_once($pomo_dir.'/inc/interface.post-modern-field.php');
    require_once($pomo_dir.'/inc/class.post-modern-field.php');
    require_once($pomo_dir.'/inc/class.post-modern-combofield.php');
    require_once($pomo_dir.'/inc/class.post-modern-tablefield.php');
    require_once($pomo_dir.'/inc/class.post-modern-validator.php');
    // load field types
    // planned types (text, textarea, select, multiple select, combo, radio, file, image, date, date range,
    // address, time, time range, toggle, etc.
    require_once($pomo_dir.'/inc/fields/class.text-field.php');
    require_once($pomo_dir.'/inc/fields/class.select-field.php');
    require_once($pomo_dir.'/inc/fields/class.radio-field.php');
    require_once($pomo_dir.'/inc/fields/class.checkbox-field.php');
    require_once($pomo_dir.'/inc/fields/class.textarea-field.php');
    require_once($pomo_dir.'/inc/fields/class.date-field.php');
    require_once($pomo_dir.'/inc/fields/class.post-field.php');
    require_once($pomo_dir.'/inc/fields/class.color-field.php');
    require_once($pomo_dir.'/inc/fields/class.address-field.php');
    require_once($pomo_dir.'/inc/fields/class.daterange-field.php');
    require_once($pomo_dir.'/inc/fields/class.geopoint-field.php');
}


/*

= QUESTIONS: 
1. Do we need to create a global config system for PostModern for things like text domain (i18n)?
*/