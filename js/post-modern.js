jQuery(document).ready(function($) {
    // force metaboxes with invalid field to be open
    $('.postbox.closed').each(function(){
        if( $('.form_field.invalid', this).length ) $(this).removeClass('closed');
    });
    
    // set up chosen on select fields
    if( typeof $().chosen !== 'undefined' ){
        // select fields (have to be drawn on open boxes only)
        $('.postbox:not(.closed) select.form_field_input').chosen({allow_single_deselect: true});
        $('.postbox.closed').click(function(){
            // draw them as the box opens
            $('select.form_field_input:not(.chzn-done)', this).chosen({allow_single_deselect: true});
        });
        $('.postbox select.form_field_input').change( function(){$(this).trigger('liszt:updated')});
	}
	
	// date fields
    if( typeof $().datepicker != 'undefined' ){
        $('.date_input').each( function(){
        	var options = {};
        	options.constrainInput = parseInt($(this).attr('data-constrain'));
        	options.minDate = $(this).attr('data-mindate');
        	options.maxDate = $(this).attr('data-maxdate');
        	$(this).datepicker(options);
        });
    }
    
    // color fields, cribbed from Twenty Eleven theme-options.js 
    if( typeof $().farbtastic != 'undefined' ){
        $('.color_field').each(function(){	
    
    		var pickColor = function(a) {
    			farbtastic.setColor(a);
    			field.val(a);
    			field.change();
    			colorSample.css('background-color', a);
    		};
    
        	var field = $('.color_input',this);
        	var colorPicker = $('.color_picker', this);
        	var colorSample = $('.color_sample',this);
        	var pickerButton = $('.color_picker_button');
        	var farbtastic = $.farbtastic( colorPicker, pickColor); 
        	
        	pickColor( field.val() );
        	
    		
    		var checkColor = function(){
    			if( !field.val() ){
    				colorSample.addClass('empty');
    				colorSample.css('background','none');
    			} else {
    				colorSample.removeClass('empty');
    			}
    		}
    		checkColor();
        	field.change( function(){checkColor();} );
    		field.keyup( function() {
    			var a = field.val(),
    				b = a;
    			a = a.replace(/[^a-fA-F0-9]/, '');
    			if ( '#' + a !== b ){
    				field.val(a);
    				checkColor();
                }
    			if ( a.length === 3 || a.length === 6 ){
    				pickColor( '#' + a );
                    checkColor();
                }
    		});
    		
    		colorSample.click( function(e) {
    			colorPicker.show();
    			e.preventDefault();
    		});
    
    		pickerButton.click( function(e) {
    			colorPicker.show();
    			e.preventDefault();
    		});
    		
    		$(document).mousedown( function() {
    			colorPicker.hide();
    		});
        });
    }
    
    // posts field quick add
    $('.form_field.post_field').each( function(){
		var this_field = this;
		// if we have a quick add button
        if($('a.new-post', this_field ).length){
            toggleQuickAdd = function(e,context){
    			if( typeof context == 'undefined' ) context = this_field;
    			$('a.new-post', context).toggleClass('showing');
        		$(' + .quick-add-post', context).slideToggle();
                // set the focus
    			if( $('a.new-post', context).hasClass('showing') ) 
                    $(' + .quick-add-post .new-post-title', context).focus();
    
        		return false;
            }
    
        	$('a.new-post', this_field ).click(toggleQuickAdd);
        	$('+ .quick-add-post .cancel-action', this_field).click(toggleQuickAdd);
        	
        	
        	$('+ .quick-add-post .quick-add-submit', this_field).click(function(){
                var new_title = $(' + .quick-add-post .new-post-title', this_field).val();    
                if( new_title ){
                    //var data = PoMoAjax[ $('select.post_input', this_field).attr('id') ];
	                var new_type = $(' + .quick-add-post .new-post-type', this_field).val();    
	                var new_status = $(' + .quick-add-post .new-post-status', this_field).val();
	                var new_nonce = $(' + .quick-add-post .new-post-nonce', this_field).val();        
	            	$(this).attr('disabled','disabled');
	            	$('+ .quick-add-post', this_field).addClass('waiting-response');
                    $.post(
                        ajaxurl,
                        {
                            action : 'pomo_add_post',
                            post_title : new_title,
                            quick_add_nonce : new_nonce,
                            post_type: new_type,
                            post_status: new_status
                        },
                        function( response ) {
			            	if( response.ID ) {
				            	$('+ .quick-add-post', this_field).removeClass('waiting-response');				            	
								$('+ .quick-add-post .new-post-title', this_field).val('');
								$('+ .quick-add-post .quick-add-submit', this_field).removeAttr('disabled');
				            	toggleQuickAdd(null,this_field);
				            	addOption($('select.post_input', this_field),response.ID,response.post_title,true);
				            } else{
								$('+ .quick-add-post .error', this_field).css('display','block');
				            }
                        }
                    );
                }
            });
        }    	
    	
    	
    });
    // update a selector with a new option, this should probably be a jQuery extension so we get
    //  that sweet syntactic sugar like this $(selector).addOption(value,label,isSelected)
    var addOption = function(selector,newValue,newLabel,isSelected){
		var selectField = $(selector);
		// did we catch something?
		if( !selectField.length ) return;

    	var opt = $('<option/>').val(newValue).html(newLabel);
		if( isSelected ){
			opt.attr('selected','selected');
	    	if( !selectField.attr('multiple') ){
	    		selectField.val('');
	    	}
		}
    	selectField.append( opt );
    	// whenever we change a form field, we always trigger a change event
    	selectField.change();
    	return selectField;
    }
    
    // Array fields
    //
    // 
    $('.clonable').each(function(i){
        var wrapper;
        
        if( 'TR' === this.tagName ) wrapper = 'td';
        else wrapper = 'div';
        
        $(this).append(
            '<'+wrapper+' class="clone-actions">'+
            '<button class="add-clone-button clone-button button" type="button">+</button>'+
            '<button class="delete-clone-button clone-button button" type="button">-</button>'+
            '</'+wrapper+'>'
        );
    });
    var updateTableOrder = function(el){
        $('tr',el).each(function(index){
            $('input',this).each(function(){
                this.name = this.name.replace(/\[\d\]/,'['+index+']');
                this.id = this.id.replace(/_\d_/,'_'+index+'_');          
            });
        });
    }
    var addCloneClicked = function(){
        var clonable,clone;

        clonable = $(this).parent().parent();
		clone = clonable.clone().addClass('clone');
		console.log( clone );
		$('input[type=text]', clone).val('');
        clonable.after(clone);
        if( 'TR' === clonable.get(0).tagName ){
            updateTableOrder( $(clonable).parent() );
        }
        addCloneControls();
    }
    var deleteCloneClicked = function(){
        var clonable,isTable;
        
        clonable = $(this).parent().parent();
        if( clonable.hasClass('clone') ){
            isTable = 'TR' === clonable.get(0).tagName;
            $(clonable).remove();
            if( isTable ){
                updateTableOrder( $(clonable).parent() );
            }
        } else{
            $('input', clonable ).val(''); 
        }
    }
    var addCloneControls = function(){
        $('.add-clone-button').unbind();
        $('.add-clone-button').click(addCloneClicked);
        $('.delete-clone-button').unbind();
        $('.delete-clone-button').click(deleteCloneClicked);
    }
    addCloneControls();
});